package com.tendersoftware.crmtendersoftware.utils;

import com.tendersoftware.crmtendersoftware.models.leads.DataModel;

import java.util.Comparator;

/**
 * Created by SYS on 10/3/2017.
 */

public class NameComparator implements Comparator<DataModel> {

    private String keyWord;
    public NameComparator(String keyWord){
        this.keyWord = keyWord;
    }

    @Override
    public int compare(DataModel dataModel, DataModel t1) {
        if(dataModel.getStatus_name().startsWith(keyWord)) {
            return t1.getStatus_name().startsWith(keyWord)? dataModel.getStatus_name().compareTo(t1.getStatus_name()): -1;
        } else {
            return t1.getStatus_name().startsWith(keyWord)? 1: dataModel.getStatus_name().compareTo(t1.getStatus_name());
        }
    }
}
