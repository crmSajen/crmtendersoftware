package com.tendersoftware.crmtendersoftware.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.tendersoftware.crmtendersoftware.MainActivity;
import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.activities.LoginActivity;
import com.tendersoftware.crmtendersoftware.callrecord.Main2Activity;
import com.tendersoftware.crmtendersoftware.callrecord.TService;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.interfaces.AlertDialogInterface;
import com.tendersoftware.crmtendersoftware.session.CRMSessionConstants;
import com.tendersoftware.crmtendersoftware.session.CRMSharedPreference;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Utilities {

    private static Utilities ourInstance = new Utilities();

    public static Utilities getInstance() {
        return ourInstance;
    }

    private Utilities() {
    }

    public boolean isNetworkAvailable(Context context) {
        if (context != null) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }
        return false;
    }

    public void closeKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public boolean isValidEmail(CharSequence target) {
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(target);
        return matcher.matches();
    }

    public void makeCall(final Activity activity, final String url) {
        if (!Dexter.isRequestOngoing())
            Dexter.checkPermission(new PermissionListener() {
                @Override
                public void onPermissionGranted(PermissionGrantedResponse response) {
                    Intent tel = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                    try {
                        activity.startActivity(tel);
                        activity.startService(new Intent(activity, TService.class));
                        activity.finish();
                    } catch (SecurityException e) {
                    }
                }

                @Override
                public void onPermissionDenied(PermissionDeniedResponse response) {
                    Toast.makeText(activity, "Permission is denied", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                    token.continuePermissionRequest();
                }
            }, android.Manifest.permission.CALL_PHONE);
    }


    public void sendEmail(Activity activity, String email) {
        String body = " ";
        Intent mail = new Intent(Intent.ACTION_SEND);
        mail.setType("application/octet-stream");
        mail.putExtra(Intent.EXTRA_EMAIL, new String[]{email.replaceAll("mailto:", "").replaceAll("%40", "@")});
        mail.putExtra(Intent.EXTRA_SUBJECT, " ");
        mail.putExtra(Intent.EXTRA_TEXT, body);
        activity.startActivity(mail);
    }

    public void showToast(Activity activity, int text) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
    }


    public void closeKeyboard(Activity activity, Dialog dialog) {
        View view = dialog.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showSnackBarShort(final Context context, String message, View view) {

        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");

        Snackbar snackbar;
        snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.app_color));
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
        textView.setTypeface(font);
        snackbar.show();
    }

    public static LoadingView loadingView(FragmentActivity fragmentActivity, String message) {
        try {
            LoadingView mProgressHUD = new LoadingView();
            Bundle args = new Bundle();
            args.putString(Constants.DIALOG_MESSAGE, message);
            mProgressHUD.setArguments(args);
            mProgressHUD.show(fragmentActivity.getSupportFragmentManager(), "");
            mProgressHUD.setCancelable(false);
            return mProgressHUD;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void alertDialogOptions(final Activity mActivity, final String header, final String message,final int code,final AlertDialogInterface alertDialogInterface) {
        new SweetAlertDialog(mActivity, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(header)
                .setContentText(message)
                .setCancelText(mActivity.getString(R.string.cancel))
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .setConfirmText(mActivity.getResources().getString(R.string.ok))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        alertDialogInterface.clickedOkButton(code);
                    }
                })
                .show();
    }

    public static void alertDialogSingleOptions(final Activity mActivity, final String header) {
//        new SweetAlertDialog(mActivity, SweetAlertDialog.NORMAL_TYPE)
//                .setTitleText(header)
//                .setContentText(message)
//                .setCancelText(mActivity.getString(R.string.ok))
//                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sweetAlertDialog) {
//                        CRMSharedPreference.getInstance(mActivity).removeValues(CRMSessionConstants.UID);
//                        CRMSharedPreference.getInstance(mActivity).removeValues(CRMSessionConstants.IS_LOGIN);
//                        CRMSharedPreference.getInstance(mActivity).removeValues(CRMSessionConstants.USER_ID);
//                        mActivity.startActivity(new Intent(mActivity, LoginActivity.class));
//                        sweetAlertDialog.dismissWithAnimation();
//
//                    }
//                }).show();


        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(header)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        CRMSharedPreference.getInstance(mActivity).removeValues(CRMSessionConstants.UID);
                        CRMSharedPreference.getInstance(mActivity).removeValues(CRMSessionConstants.IS_LOGIN);
                        CRMSharedPreference.getInstance(mActivity).removeValues(CRMSessionConstants.USER_ID);
//                        mActivity.startActivity(new Intent(mActivity, LoginActivity.class));
                        Intent intent = new Intent(mActivity, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mActivity.startActivity(intent);
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.BLACK);

    }


public static void playaudio(Activity mActivity, String filePath){

    MediaPlayer mediaPlayer = new MediaPlayer();


    mediaPlayer = MediaPlayer.create(mActivity, Uri.parse(filePath));
    try {
        mediaPlayer.prepare();
    } catch (IOException e) {
        e.printStackTrace();
    }
    mediaPlayer.start();

}
}
