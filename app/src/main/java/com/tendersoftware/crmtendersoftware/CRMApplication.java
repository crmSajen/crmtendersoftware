package com.tendersoftware.crmtendersoftware;

import android.app.Application;
import android.content.Context;

import com.karumi.dexter.Dexter;
import com.tendersoftware.crmtendersoftware.database.RealmDBHelper;


public class CRMApplication extends Application {

    public static CRMApplication getApplication(Context context) {
        if (context instanceof CRMApplication) {
            return (CRMApplication) context;
        }
        return (CRMApplication) context.getApplicationContext();
    }

    public CRMApplication() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Dexter.initialize(this);
        RealmDBHelper.getInstance().setDBConfiguration(getApplicationContext());
    }
}
