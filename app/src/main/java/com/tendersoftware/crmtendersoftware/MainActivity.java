package com.tendersoftware.crmtendersoftware;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuPopup;
import com.shehabic.droppy.animations.DroppyFadeInAnimation;
import com.tendersoftware.crmtendersoftware.activities.LoginActivity;
import com.tendersoftware.crmtendersoftware.callrecord.CallRecordedList;
import com.tendersoftware.crmtendersoftware.callrecord.Main2Activity;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.fragments.ConstructionFragment;
import com.tendersoftware.crmtendersoftware.fragments.TabLayoutFragment;
import com.tendersoftware.crmtendersoftware.interfaces.AlertDialogInterface;
import com.tendersoftware.crmtendersoftware.models.login.LoginModel;
import com.tendersoftware.crmtendersoftware.models.login.LogoutModel;
import com.tendersoftware.crmtendersoftware.session.CRMSessionConstants;
import com.tendersoftware.crmtendersoftware.session.CRMSharedPreference;
import com.tendersoftware.crmtendersoftware.utils.Constants;
import com.tendersoftware.crmtendersoftware.utils.CustomTypefaceSpan;
import com.tendersoftware.crmtendersoftware.utils.Utilities;
import com.tendersoftware.crmtendersoftware.webservice.WebService;
import com.tendersoftware.crmtendersoftware.webservice.interfaces.WebServiceAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, SearchView.OnQueryTextListener, AlertDialogInterface {

    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.version_text)
    TextView versionTextView;
    @BindView(R.id.logo_img)
    ImageView logoImg;
    Menu menu;

    ImageView dropDownImg;
    private MenuInflater inflaterNav;

    final List<MenuItem> items = new ArrayList<>();
    int menuPosition = 0;
    GoogleApiClient mCredentialsApiClient;
    private Interpolator interpolator;
    LoadingView loadingView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        setupWindowAnimations();
        uiSetup();
        mCredentialsApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(Auth.CREDENTIALS_API)
                .build();
    }

    private void uiSetup() {
        setSupportActionBar(toolbar);
        drawer.setScrimColor(Color.TRANSPARENT);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(1).setChecked(true);
        menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            items.add(menu.getItem(i));
            MenuItem mi = menu.getItem(i);
            applyFontToMenuItem(mi);
        }
        getSupportActionBar().setTitle("");
//        getSupportActionBar().setLogo(ContextCompat.getDrawable(this,R.drawable.logo_top));
        Fragment fragment = new TabLayoutFragment();
        Bundle bundle = new Bundle();
        bundle.putString("TO", "LEADS");
        fragment.setArguments(bundle);
        pushFragments(fragment, false);

        appVersion();

        dropDownImg = (ImageView) findViewById(R.id.drop_down);
        dropDownImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dropDown();
            }
        });
    }

    private void dropDown() {
        DroppyMenuPopup.Builder droppyBuilder = new DroppyMenuPopup.Builder(this, dropDownImg);

        DroppyMenuPopup droppyMenu = droppyBuilder.fromMenu(R.menu.header_list)
                .triggerOnAnchorClick(false)
                .setOnClick(new DroppyClickCallbackInterface() {
                    @Override
                    public void call(View v, int id) {
                        Log.d("Id:", String.valueOf(id));
                        Toast.makeText(MainActivity.this, "Menu Val " + v.getId(), Toast.LENGTH_SHORT).show();
                        v.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
                    }
                })
                .setPopupAnimation(new DroppyFadeInAnimation())
                .setXOffset(300)
                .setYOffset(380)
                .addSeparator()
                .build();

        droppyMenu.show();
    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 1, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void appVersion() {
        try {
            PackageManager manager = this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            String version = info.versionName;
            versionTextView.setText("Version : V" + version);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        switch (item.getItemId()) {
            case R.id.nav_feed:
//                toolbar.setTitle(getString(R.string.feeds));
                bundle.putString("TO", "FEEDS");
//                fragment = new TabLayoutFragment();
                fragment = new ConstructionFragment();
                fragment.setArguments(bundle);
                menuPosition = items.indexOf(item);
                break;
            case R.id.nav_lead:
//                toolbar.setTitle(getString(R.string.leads));
                fragment = new TabLayoutFragment();
                bundle.putString("TO", "LEADS");
                fragment.setArguments(bundle);
                menuPosition = items.indexOf(item);
                break;
            case R.id.nav_clients:
//                toolbar.setTitle(getString(R.string.clients));
                bundle.putString("TO", "CLIENTS");
//                fragment = new TabLayoutFragment();
                fragment = new ConstructionFragment();
                fragment.setArguments(bundle);
                menuPosition = items.indexOf(item);
                break;
            case R.id.nav_task:
//                toolbar.setTitle(getString(R.string.task));
                bundle.putString("TO", "TASKS");
                fragment = new TabLayoutFragment();
                fragment.setArguments(bundle);
                menuPosition = items.indexOf(item);

                break;
            case R.id.nav_internal:
//                toolbar.setTitle(getString(R.string.internal));
                bundle.putString("TO", "INTERNAL");
                fragment = new TabLayoutFragment();
                fragment.setArguments(bundle);
                menuPosition = items.indexOf(item);
                break;
            case R.id.nav_ticket:
//                toolbar.setTitle(getString(R.string.ticket));
                bundle.putString("TO", "TICKET");
                fragment = new TabLayoutFragment();
                fragment.setArguments(bundle);
                menuPosition = items.indexOf(item);
                break;
            case R.id.nav_dashboard:
//                toolbar.setTitle(getString(R.string.dashboard));
                fragment = new ConstructionFragment();
                menuPosition = items.indexOf(item);
                break;
            case R.id.nav_setup:
//                toolbar.setTitle(getString(R.string.setup));
                fragment = new ConstructionFragment();
                menuPosition = items.indexOf(item);
//                startActivity(new Intent(MainActivity.this, Main2Activity.class));
                break;
            case R.id.nav_help:
//                toolbar.setTitle(getString(R.string.help));
                fragment = new ConstructionFragment();
                menuPosition = items.indexOf(item);
                break;
            case R.id.nav_my_profile:
//                toolbar.setTitle(getString(R.string.my_profile));
//                fragment = new CallRecordedList();
                fragment = new ConstructionFragment();
                menuPosition = items.indexOf(item);
                break;
            case R.id.nav_edit_profile:
//                toolbar.setTitle(getString(R.string.edit_profile));
                fragment = new ConstructionFragment();
                menuPosition = items.indexOf(item);
                break;
            case R.id.nav_logout:
                logout();
                break;
        }

        pushFragments(fragment, false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        navigationView.getMenu().getItem(menuPosition).setChecked(true);

        return true;
    }

    public void pushFragments(Fragment fragment, boolean isAnimation) {
        try {
            if (fragment != null) {
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();
                if (isAnimation)
                    ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                ft.replace(R.id.container, fragment);
                ft.commitAllowingStateLoss();
                manager.executePendingTransactions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void logout() {
        Utilities.getInstance().alertDialogOptions(this, getString(R.string.logout), getString(R.string.logout_string), Constants.LOGOUT_OK, this);
    }

    @Override
    public void onBackPressed() {

        Fragment f = this.getSupportFragmentManager().findFragmentById(R.id.container);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    private void logoutApi() {
        loadingView = Utilities.loadingView(this, "Loading");
        Map<String, Object> map = new HashMap<>();
        map.put("muid", CRMSharedPreference.getInstance(this).getString(CRMSessionConstants.UID));

        LogoutModel mLogoutModel = new LogoutModel();
        mLogoutModel.setMuid(CRMSharedPreference.getInstance(this).getString(CRMSessionConstants.UID));

        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        Call<LoginModel> responseBodyCall = webServiceAPI.logout(mLogoutModel);
        responseBodyCall.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                loadingView.dismiss();
                if (response.body().getSuccess()) {
//                    logoutClient();
                    Utilities.getInstance().showSnackBarShort(MainActivity.this, "Credentials Removed", drawer);
                    CRMSharedPreference.getInstance(MainActivity.this).putString(CRMSessionConstants.USER_ID, "");
                    CRMSharedPreference.getInstance(MainActivity.this).putString(CRMSessionConstants.UID, "");
                    CRMSharedPreference.getInstance(MainActivity.this).putBoolean(CRMSessionConstants.IS_LOGIN, false);

                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Utilities.getInstance().showSnackBarShort(MainActivity.this, response.body().getError().getMessage(), drawer);
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                loadingView.dismiss();
                Utilities.getInstance().showSnackBarShort(MainActivity.this, "Something went wrong", drawer);
            }
        });
    }

    private void logoutClient() {
        Auth.CredentialsApi.disableAutoSignIn(mCredentialsApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()) {
                    Log.d("MAIN ACTIVITY", "Remove : OK");
                    Utilities.getInstance().showSnackBarShort(MainActivity.this, "Credentials Removed", drawer);
                    CRMSharedPreference.getInstance(MainActivity.this).putString(CRMSessionConstants.USER_ID, "");
                    CRMSharedPreference.getInstance(MainActivity.this).putString(CRMSessionConstants.UID, "");
                    CRMSharedPreference.getInstance(MainActivity.this).putBoolean(CRMSessionConstants.IS_LOGIN, false);

                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else
                    Utilities.getInstance().showSnackBarShort(MainActivity.this, "Failed to Remove Client", drawer);
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        searchView.setOnQueryTextListener(this);
        searchView.clearFocus();

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void clickedOkButton(int code) {
        if (code == Constants.LOGOUT_OK)
            logoutApi();
    }

    @Override
    public void clickedCancelButton(int code) {

    }
}
