package com.tendersoftware.crmtendersoftware.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sababado.circularview.CircularView;
import com.sababado.circularview.Marker;
import com.sababado.circularview.SimpleCircularViewAdapter;
import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.models.internals.InternalDataModel;
import com.tendersoftware.crmtendersoftware.models.internals.InternalModel;
import com.tendersoftware.crmtendersoftware.models.leads.DataModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.github.francoiscampbell.circlelayout.CircleLayout;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by SYS on 10/3/2017.
 */

public class InternalAdapter extends RecyclerView.Adapter<InternalAdapter.MyViewHolder> {

    private List<InternalDataModel> data = new ArrayList<>();
    private Activity activity;
    private MySimpleCircularViewAdapter mySimpleCircularViewAdapter;

    public InternalAdapter(List<InternalDataModel> data, Activity activity) {
        this.data = data;
        this.activity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.group_img)
        CircleLayout circularView;
        @BindView(R.id.name_text)
        TextView mGroupNameText;
        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public InternalAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.internal_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

//        mySimpleCircularViewAdapter = new MySimpleCircularViewAdapter();
//        holder.circularView.setAdapter(mySimpleCircularViewAdapter);

//        CircleImageView circleImageView = new CircleImageView(activity);
//        CircleImageView circleImageView1 = new CircleImageView(activity);
//        CircleImageView circleImageView2 = new CircleImageView(activity);
//
//        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(com.intuit.sdp.R.dimen._18sdp, com.intuit.sdp.R.dimen._18sdp);
//        circleImageView.setLayoutParams(layoutParams);
//        circleImageView1.setLayoutParams(layoutParams);
//        circleImageView2.setLayoutParams(layoutParams);
//
//        circleImageView.setImageResource(R.drawable.hugh);
//        circleImageView1.setImageResource(R.drawable.hugh);
//        circleImageView2.setImageResource(R.drawable.hugh);
//
//        holder.circularView.addView(circleImageView);
//        holder.circularView.addView(circleImageView1);
//        holder.circularView.addView(circleImageView2);

//        circularView(holder.relativeLayout);
        Log.d(TAG,"groupTextname "+data.get(position).getGroup_name()+"  "+data.get(position).getId()+"   "+data.get(position).getNotification_count());
        holder.mGroupNameText.setText(data.get(position).getGroup_name());


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MySimpleCircularViewAdapter extends SimpleCircularViewAdapter {
        @Override
        public int getCount() {
            // This count will tell the circular view how many markers to use.
            return 3;
        }

        @Override
        public void setupMarker(int position, Marker marker) {

            marker.setSrc(R.drawable.hugh);
            marker.setFitToCircle(true);
            marker.setRadius(35);
        }
    }
}
