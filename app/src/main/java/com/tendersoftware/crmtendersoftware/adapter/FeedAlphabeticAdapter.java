package com.tendersoftware.crmtendersoftware.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.activities.CommentActivity;
import com.tendersoftware.crmtendersoftware.models.feeds.DataModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by SYS on 11/29/2017.
 */

public class FeedAlphabeticAdapter extends RecyclerView.Adapter<FeedAlphabeticAdapter.MyViewHolder> {

    private List<DataModel> data = new ArrayList<>();

    private Activity activity;


    public FeedAlphabeticAdapter(List<DataModel> data,Activity activity) {
        this.data = data;
        this.activity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_text)
        TextView nametextView;
        @BindView(R.id.user_img)
        ImageView userImageView;
        @BindView(R.id.feed_text)
        TextView feedTextView;
        @BindView(R.id.time_text)
        TextView timeTextView;
        @BindView(R.id.like_text)
        TextView likeTextView;
        @BindView(R.id.comment_text)
        TextView commentTextView;
        @BindView(R.id.create_ticket_text)
        TextView creatTicketTextView;
        @BindView(R.id.addtask_text)
        TextView addTaskTextView;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public FeedAlphabeticAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feeds_row, parent, false);

        return new FeedAlphabeticAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FeedAlphabeticAdapter.MyViewHolder holder, final int position) {

        if(data.get(position).getFrom_user_name()!=null)
            holder.nametextView.setText(data.get(position).getFrom_user_name());
        else
            holder.nametextView.setText("");
        if (data.get(position).getTitle_des()!=null)
            holder.feedTextView.setText(data.get(position).getTitle_des());
        else
            holder.feedTextView.setText("");

        holder.timeTextView.setText(data.get(position).getTimeago());


        if (data.get(position).getFrom_user_image()!=null)
            Glide.with(activity).load(data.get(position).getFrom_user_image()).into(holder.userImageView);
        holder.commentTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("module", "feed");
                b.putInt("module_id",data.get(position).getFeed_id());
                Intent in = new Intent(activity, CommentActivity.class);
                in.putExtras(b);
                activity.startActivity(in);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
