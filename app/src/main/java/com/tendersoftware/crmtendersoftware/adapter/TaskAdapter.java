package com.tendersoftware.crmtendersoftware.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.models.leads.DataModel;
import com.tendersoftware.crmtendersoftware.models.tasks.TaskDataModel;
import com.tendersoftware.crmtendersoftware.models.tasks.TaskModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by SYS on 10/3/2017.
 */

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.MyViewHolder> {

    private List<TaskDataModel> data = new ArrayList<>();

    private Activity activity;



    public TaskAdapter(List<TaskDataModel> data, Activity activity) {
        this.data = data;
        this.activity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_text)
        TextView mNameTextView;
        @BindView(R.id.company_text)
        TextView mCompanyNameTextView;
        @BindView(R.id.assigned_to_text)
        TextView mAssignedToTextView;
        @BindView(R.id.assigned_by_text)
        TextView mAssignedByTextView;
        @BindView(R.id.date_reminder_text)
        TextView mReminderTextView;
        @BindView(R.id.date_due_text)
        TextView mDueDateTextView;
        @BindView(R.id.date_created_text)
        TextView mCreatedTextView;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public TaskAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.mNameTextView.setText(data.get(position).getName());


    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
