package com.tendersoftware.crmtendersoftware.adapter;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.activities.CommentActivity;
import com.tendersoftware.crmtendersoftware.models.clients.ClientDataModel;
import com.tendersoftware.crmtendersoftware.models.leads.DataModel;
import com.tendersoftware.crmtendersoftware.utils.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by SYS on 11/24/2017.
 */

public class ClientAdapter extends RecyclerView.Adapter<ClientAdapter.MyViewHolder> {

    private List<ClientDataModel> data = new ArrayList<>();

    private Activity activity;
    private ClientAdapter.OnItemClickListener onItemClickListener;



    public void setOnItemClickListener(ClientAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, String val);
    }

    public ClientAdapter(List<ClientDataModel> data, Activity activity) {
        this.data = data;
        this.activity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_text)
        TextView nametextView;
        @BindView(R.id.company_text)
        TextView companyTextView;
        @BindView(R.id.email_text)
        TextView emailTextView;
        @BindView(R.id.phone_text)
        TextView phoneTextView;
        @BindView(R.id.skype_text)
        TextView skypeTextView;
        @BindView(R.id.lead_count_text)
        TextView leadCountTextView;
        @BindView(R.id.created_text)
        TextView createdTextView;
        @BindView(R.id.lock_img)
        ImageView lockImageView;
        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public ClientAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lead_row, parent, false);

        return new ClientAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ClientAdapter.MyViewHolder holder, final int position) {

        if(data.get(position).getName()!=null) {
            holder.nametextView.setVisibility(View.VISIBLE);
            holder.nametextView.setText(data.get(position).getName());
        }
        else
            holder.nametextView.setVisibility(View.INVISIBLE);

        if (data.get(position).getSales_unit()!=null) {
            holder.companyTextView.setVisibility(View.VISIBLE);
            holder.companyTextView.setText(data.get(position).getSales_unit());
        }
        else
            holder.companyTextView.setVisibility(View.INVISIBLE);

        if (data.get(position).getEmail()!=null && data.get(position).getEmail().toString().trim().length()>0) {
            holder.emailTextView.setVisibility(View.VISIBLE);
            holder.emailTextView.setText(data.get(position).getEmail());
        }
        else
            holder.emailTextView.setVisibility(View.INVISIBLE);

        if (data.get(position).getSkype()!=null && data.get(position).getSkype().toString().trim().length()>0) {
            holder.skypeTextView.setVisibility(View.VISIBLE);
            holder.skypeTextView.setText(data.get(position).getSkype());
        }
        else
            holder.skypeTextView.setVisibility(View.INVISIBLE);

        if (data.get(position).getPhonenumber()!=null && data.get(position).getPhonenumber().toString().trim().length()>0) {
            holder.phoneTextView.setVisibility(View.VISIBLE);
            holder.phoneTextView.setText(data.get(position).getPhonenumber());
        }
        else
            holder.phoneTextView.setVisibility(View.INVISIBLE);

        if (data.get(position).getStatus_name()!=null && data.get(position).getStatus_name().toString().trim().length()>0) {
            holder.leadCountTextView.setVisibility(View.VISIBLE);
            holder.leadCountTextView.setText(data.get(position).getStatus_name());
            holder.leadCountTextView.setTag(data.get(position).getStatus_name());
        }
        else{
            holder.leadCountTextView.setVisibility(View.INVISIBLE);
            holder.lockImageView.setVisibility(View.INVISIBLE);
        }

        if (data.get(position).getDateadded()!=null) {
            String shortTimeStr = "";
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                Date date = null;
                date = simpleDateFormat.parse(data.get(position).getDateadded());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                shortTimeStr = sdf.format(date);

                shortTimeStr = "Created: "+shortTimeStr;

            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.createdTextView.setVisibility(View.VISIBLE);
            holder.createdTextView.setText(shortTimeStr);
        }
        else
            holder.createdTextView.setVisibility(View.INVISIBLE);

        if (data.get(position).getColor()!=null) {
            GradientDrawable shape =  (GradientDrawable)holder.leadCountTextView.getBackground();
            shape.setColor(Color.parseColor(data.get(position).getColor()));
            holder.leadCountTextView.setBackground(shape);
        }

        holder.emailTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.getInstance().sendEmail(activity,data.get(position).getEmail().toString());
            }
        });

        holder.phoneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.getInstance().makeCall(activity,"tel:"+data.get(position).getPhonenumber().toString());
            }
        });

        holder.skypeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                skypeFunction(position);
            }
        });

        holder.leadCountTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClick(view, String.valueOf(view.getTag()));
            }
        });
holder.lockImageView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Bundle b = new Bundle();
        b.putString("module", "client");
        b.putInt("module_id",data.get(position).getClientid());
        Intent in = new Intent(activity, CommentActivity.class);
        in.putExtras(b);
        activity.startActivity(in);
    }
});
    }

    private void skypeFunction(int position){
        String marketUrl = "http://play.google.com/store/apps/details?id=";

        String mySkypeUri = "skype:"+data.get(position).getSkype()+"?call&video=true";
        if (!isSkypeInstalled(activity)) {
            //if skype is not installed than it will redirect to google play store
            Uri marketUri = Uri.parse(marketUrl+"com.skype.raider");
            Intent intent = new Intent(Intent.ACTION_VIEW, marketUri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
            return;
        }
        Uri skypeUri = Uri.parse(mySkypeUri);
        Intent intent = new Intent(Intent.ACTION_VIEW, skypeUri);
        intent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private boolean isSkypeInstalled(Context context) {
        PackageManager myPackageMgr = context.getPackageManager();
        try {
            myPackageMgr.getPackageInfo("com.skype.raider", PackageManager.GET_ACTIVITIES);
        }
        catch (PackageManager.NameNotFoundException e) {
            return (false);
        }
        return (true);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
