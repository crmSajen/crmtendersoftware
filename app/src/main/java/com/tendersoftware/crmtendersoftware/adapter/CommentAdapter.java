package com.tendersoftware.crmtendersoftware.adapter;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.models.comments.CommentDataModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

/**
 * Created by SYS on 11/24/2017.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {
    private Activity activity;
    private List<CommentDataModel> mCommentDataModel;

    public CommentAdapter(FragmentActivity activity, List<CommentDataModel> mCommentDataModel) {
        this.activity = activity;
        this.mCommentDataModel = mCommentDataModel;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_listitem, parent, false);

        return new CommentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.d(TAG,"desCription ::::: "+mCommentDataModel.get(position).getNoteDetails().getDescription());
        holder.mCommentDescriptionText.setText(mCommentDataModel.get(position).getNoteDetails().getDescription());


    }

    @Override
    public int getItemCount() {
        return mCommentDataModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name_text)
        TextView mNameText;
        @BindView(R.id.comment_description_text)
        TextView mCommentDescriptionText;
        @BindView(R.id.time_text)
        TextView mTimeText;
        @BindView(R.id.like_text)
        TextView mLikeText;
        @BindView(R.id.comment_text)
        TextView mCommentText;
        @BindView(R.id.like_image)
        ImageView mLikeImage;
        @BindView(R.id.comment_image)
        ImageView mCommentImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
