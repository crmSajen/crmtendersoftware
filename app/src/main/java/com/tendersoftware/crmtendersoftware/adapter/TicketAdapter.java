package com.tendersoftware.crmtendersoftware.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.models.leads.DataModel;
import com.tendersoftware.crmtendersoftware.models.tickets.TicketDataModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by SYS on 10/4/2017.
 */

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.MyViewHolder> {

    private List<TicketDataModel> data = new ArrayList<>();

    private Activity activity;



    public TicketAdapter(List<TicketDataModel> data, Activity activity) {
        this.data = data;
        this.activity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name_text)
        TextView mNameText;
        @BindView(R.id.company_text)
        TextView mCompanyName;
        @BindView(R.id.assigned_to_text)
        TextView mAssignedToText;
        @BindView(R.id.date_reminder_text)
        TextView mDateRemainderText;
        @BindView(R.id.date_due_text)
        TextView mDueDateText;
        @BindView(R.id.date_created_text)
        TextView mCreatedDateText;
        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ticket_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.mNameText.setText(data.get(position).getSubject());
        holder.mDateRemainderText.setText(data.get(position).getTblticketstatusName());
        holder.mDueDateText.setText(data.get(position).getTblprioritiesName());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
