package com.tendersoftware.crmtendersoftware.adapter;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.utils.Utilities;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by SYS on 11/17/2017.
 */

public class CallRecordAdapter extends RecyclerView.Adapter<CallRecordAdapter.MyViewHolder> {

    private ArrayList<String> mRecordList = new ArrayList<String>();
    private Activity mActivity;

    public CallRecordAdapter(ArrayList<String> mRecordedList, FragmentActivity activity) {
        this.mRecordList = mRecordedList;
        this.mActivity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_text)
        TextView nametextView;
        @BindView(R.id.main_listitem_layout)
        LinearLayout mMainItemLayout;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public CallRecordAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.call_record_listitem, parent, false);
        return new CallRecordAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CallRecordAdapter.MyViewHolder holder, final int position) {
        holder.nametextView.setText(mRecordList.get(position).toString());

        holder.mMainItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                if (mediaPlayer.isPlaying())
                    mediaPlayer.stop();
                String filePath;
                filePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/TendersoftRecord/"+mRecordList.get(position).toString();
                Log.d(TAG,"Clicked Item  " +filePath);
//                Utilities.playaudio(mActivity,filePath);

                try {
                    Log.d(TAG,"Clicked Item try one " +filePath);

                    mediaPlayer.setDataSource(filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    Log.d(TAG,"Clicked Item try two " +filePath);

                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Toast.makeText(mActivity, "audio playing", Toast.LENGTH_SHORT).show();
//                mediaPlayer = MediaPlayer.create(mActivity, Uri.parse(filePath));

                mediaPlayer.start();



            }
        });
    }

    @Override
    public int getItemCount() {
        return mRecordList.size();
    }
}
