package com.tendersoftware.crmtendersoftware.models.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 11/21/2017.
 */

public class LogoutModel {

    @SerializedName("muid")
    @Expose
    private String muid;

    public String getMuid() {
        return muid;
    }

    public void setMuid(String muid) {
        this.muid = muid;
    }
}
