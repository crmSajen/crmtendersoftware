package com.tendersoftware.crmtendersoftware.models.tickets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tendersoftware.crmtendersoftware.models.internals.InternalDataModel;

import java.util.List;

/**
 * Created by SYS on 11/20/2017.
 */

public class TicketDataModel {

    @SerializedName("tblticketsTicketid")
    @Expose
    private Integer tblticketsTicketid;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("tblticketassignmentsStaffid")
    @Expose
    private String tblticketassignmentsStaffid;
    @SerializedName("tblticketstatusName")
    @Expose
    private String tblticketstatusName;
    @SerializedName("tblprioritiesName")
    @Expose
    private String tblprioritiesName;
    @SerializedName("tblticketsDate")
    @Expose
    private String tblticketsDate;




    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    public Integer getTblticketsTicketid() {
        return tblticketsTicketid;
    }

    public void setTblticketsTicketid(Integer tblticketsTicketid) {
        this.tblticketsTicketid = tblticketsTicketid;
    }

    public String getTblticketassignmentsStaffid() {
        return tblticketassignmentsStaffid;
    }

    public void setTblticketassignmentsStaffid(String tblticketassignmentsStaffid) {
        this.tblticketassignmentsStaffid = tblticketassignmentsStaffid;
    }

    public String getTblticketstatusName() {
        return tblticketstatusName;
    }

    public void setTblticketstatusName(String tblticketstatusName) {
        this.tblticketstatusName = tblticketstatusName;
    }

    public String getTblprioritiesName() {
        return tblprioritiesName;
    }

    public void setTblprioritiesName(String tblprioritiesName) {
        this.tblprioritiesName = tblprioritiesName;
    }

    public String getTblticketsDate() {
        return tblticketsDate;
    }

    public void setTblticketsDate(String tblticketsDate) {
        this.tblticketsDate = tblticketsDate;
    }
}
