package com.tendersoftware.crmtendersoftware.models.clients;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tendersoftware.crmtendersoftware.models.leads.DataModel;

import java.util.List;

/**
 * Created by SYS on 11/24/2017.
 */

public class ClientResultModel {

    @SerializedName("data")
    @Expose
    private List<ClientDataModel> data = null;

    public List<ClientDataModel> getData() {
        return data;
    }

    public void setData(List<ClientDataModel> data) {
        this.data = data;
    }

}
