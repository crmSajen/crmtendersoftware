package com.tendersoftware.crmtendersoftware.models.feeds;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 9/21/2017.
 */

public class ReplyModel {

    @SerializedName("reply_task_id")
    @Expose
    private String reply_task_id;
    @SerializedName("reply_comment_id")
    @Expose
    private String reply_comment_id;
    @SerializedName("from_reply_user_id")
    @Expose
    private String from_reply_user_id;
    @SerializedName("from_reply_user_image")
    @Expose
    private String from_reply_user_image;
    @SerializedName("from_reply_user_name")
    @Expose
    private String from_reply_user_name;
    @SerializedName("reply_comment_des")
    @Expose
    private String reply_comment_des;
    @SerializedName("reply_comments_timeago")
    @Expose
    private String reply_comments_timeago;
    @SerializedName("reply_comments_date")
    @Expose
    private String reply_comments_date;
//    @SerializedName("reply_like_data")
//    @Expose
//    private ReplyLikeDataModel replyLikeData;


    public String getReply_task_id() {
        return reply_task_id;
    }

    public void setReply_task_id(String reply_task_id) {
        this.reply_task_id = reply_task_id;
    }

    public String getReply_comment_id() {
        return reply_comment_id;
    }

    public void setReply_comment_id(String reply_comment_id) {
        this.reply_comment_id = reply_comment_id;
    }

    public String getFrom_reply_user_id() {
        return from_reply_user_id;
    }

    public void setFrom_reply_user_id(String from_reply_user_id) {
        this.from_reply_user_id = from_reply_user_id;
    }

    public String getFrom_reply_user_image() {
        return from_reply_user_image;
    }

    public void setFrom_reply_user_image(String from_reply_user_image) {
        this.from_reply_user_image = from_reply_user_image;
    }

    public String getFrom_reply_user_name() {
        return from_reply_user_name;
    }

    public void setFrom_reply_user_name(String from_reply_user_name) {
        this.from_reply_user_name = from_reply_user_name;
    }

    public String getReply_comment_des() {
        return reply_comment_des;
    }

    public void setReply_comment_des(String reply_comment_des) {
        this.reply_comment_des = reply_comment_des;
    }

    public String getReply_comments_timeago() {
        return reply_comments_timeago;
    }

    public void setReply_comments_timeago(String reply_comments_timeago) {
        this.reply_comments_timeago = reply_comments_timeago;
    }

    public String getReply_comments_date() {
        return reply_comments_date;
    }

    public void setReply_comments_date(String reply_comments_date) {
        this.reply_comments_date = reply_comments_date;
    }
}
