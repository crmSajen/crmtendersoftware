package com.tendersoftware.crmtendersoftware.models.tickets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tendersoftware.crmtendersoftware.models.internals.InternalDataModel;

import java.util.List;

/**
 * Created by SYS on 11/20/2017.
 */

public class TicketResultModel {

    @SerializedName("data")
    @Expose
    private List<TicketDataModel> data = null;

    public List<TicketDataModel> getData() {
        return data;
    }

    public void setData(List<TicketDataModel> data) {
        this.data = data;
    }
}
