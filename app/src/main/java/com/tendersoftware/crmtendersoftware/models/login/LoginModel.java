package com.tendersoftware.crmtendersoftware.models.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 9/14/2017.
 */

public class LoginModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("result")
    @Expose
    private LoginResultModel result;

    @SerializedName("error")
    @Expose
    private LoginErrorModel error;

    public LoginErrorModel getError() {
        return error;
    }

    public void setError(LoginErrorModel error) {
        this.error = error;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public LoginResultModel getResult() {
        return result;
    }

    public void setResult(LoginResultModel result) {
        this.result = result;
    }

}
