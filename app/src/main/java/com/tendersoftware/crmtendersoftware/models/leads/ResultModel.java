package com.tendersoftware.crmtendersoftware.models.leads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SYS on 9/20/2017.
 */

public class ResultModel {

    @SerializedName("data")
    @Expose
    private List<DataModel> data = null;

    public List<DataModel> getData() {
        return data;
    }

    public void setData(List<DataModel> data) {
        this.data = data;
    }
}
