package com.tendersoftware.crmtendersoftware.models.clients;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 11/24/2017.
 */

public class ClientDataModel {

    @SerializedName("clientid")
    @Expose
    private Integer clientid;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phonenumber")
    @Expose
    private String phonenumber;
    @SerializedName("skype")
    @Expose
    private String skype;
    @SerializedName("sales_unit")
    @Expose
    private String sales_unit;
    @SerializedName("status_name")
    @Expose
    private String status_name;
    @SerializedName("dateadded")
    @Expose
    private String dateadded;
    @SerializedName("color")
    @Expose
    private String color;

    public Integer getClientid() {
        return clientid;
    }

    public void setClientid(Integer clientid) {
        this.clientid = clientid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getSales_unit() {
        return sales_unit;
    }

    public void setSales_unit(String sales_unit) {
        this.sales_unit = sales_unit;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getDateadded() {
        return dateadded;
    }

    public void setDateadded(String dateadded) {
        this.dateadded = dateadded;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
