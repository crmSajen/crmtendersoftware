package com.tendersoftware.crmtendersoftware.models.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 9/14/2017.
 */

public class LoginResultModel {
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("isadmin")
    @Expose
    private Boolean isadmin;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("salesunit")
    @Expose
    private String salesunit;
    @SerializedName("salesunitId")
    @Expose
    private String salesunitId;

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Boolean getIsadmin() {
        return isadmin;
    }

    public void setIsadmin(Boolean isadmin) {
        this.isadmin = isadmin;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getSalesunit() {
        return salesunit;
    }

    public void setSalesunit(String salesunit) {
        this.salesunit = salesunit;
    }

    public String getSalesunitId() {
        return salesunitId;
    }

    public void setSalesunitId(String salesunitId) {
        this.salesunitId = salesunitId;
    }
}
