package com.tendersoftware.crmtendersoftware.models.internals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 11/20/2017.
 */

public class InternalModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("result")
    @Expose
    private InternalResultModel result;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public InternalResultModel getResult() {
        return result;
    }

    public void setResult(InternalResultModel result) {
        this.result = result;
    }
}
