package com.tendersoftware.crmtendersoftware.models.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 11/24/2017.
 */

public class CommentNoteDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("leadid")
    @Expose
    private Integer leadid;
    @SerializedName("staffid")
    @Expose
    private Integer staffid;
    @SerializedName("tousertype")
    @Expose
    private String tousertype;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("dateadded")
    @Expose
    private String dateadded;
    @SerializedName("datemodified")
    @Expose
    private String datemodified;

    @SerializedName("note_type")
    @Expose
    private String note_type;
    @SerializedName("duedate")
    @Expose
    private String duedate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLeadid() {
        return leadid;
    }

    public void setLeadid(Integer leadid) {
        this.leadid = leadid;
    }

    public Integer getStaffid() {
        return staffid;
    }

    public void setStaffid(Integer staffid) {
        this.staffid = staffid;
    }

    public String getTousertype() {
        return tousertype;
    }

    public void setTousertype(String tousertype) {
        this.tousertype = tousertype;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateadded() {
        return dateadded;
    }

    public void setDateadded(String dateadded) {
        this.dateadded = dateadded;
    }

    public String getDatemodified() {
        return datemodified;
    }

    public void setDatemodified(String datemodified) {
        this.datemodified = datemodified;
    }

    public String getNote_type() {
        return note_type;
    }

    public void setNote_type(String note_type) {
        this.note_type = note_type;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }
}
