package com.tendersoftware.crmtendersoftware.models.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 11/24/2017.
 */

public class CommentsGetRequestModel {

    @SerializedName("muid")
    @Expose
    private String muid;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("module")
    @Expose
    private String module;
    @SerializedName("limit")
    @Expose
    private String limit;

    public String getMuid() {
        return muid;
    }

    public void setMuid(String muid) {
        this.muid = muid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }
}
