package com.tendersoftware.crmtendersoftware.models.feeds;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 9/21/2017.
 */

public class ReplyLikeDataModel {

    @SerializedName("note_id")
    @Expose
    private String note_id;
    @SerializedName("comment_id")
    @Expose
    private String comment_id;
    @SerializedName("likecount")
    @Expose
    private Integer likecount;
    @SerializedName("stafflike_post_label")
    @Expose
    private String stafflike_post_label;
    @SerializedName("stafflike_post")
    @Expose
    private Integer stafflike_post;


    public Integer getLikecount() {
        return likecount;
    }

    public void setLikecount(Integer likecount) {
        this.likecount = likecount;
    }

    public String getNote_id() {
        return note_id;
    }

    public void setNote_id(String note_id) {
        this.note_id = note_id;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getStafflike_post_label() {
        return stafflike_post_label;
    }

    public void setStafflike_post_label(String stafflike_post_label) {
        this.stafflike_post_label = stafflike_post_label;
    }

    public Integer getStafflike_post() {
        return stafflike_post;
    }

    public void setStafflike_post(Integer stafflike_post) {
        this.stafflike_post = stafflike_post;
    }
}
