package com.tendersoftware.crmtendersoftware.models.feeds;

import android.support.annotation.NonNull;

import java.util.Comparator;

/**
 * Created by SYS on 11/29/2017.
 */

public class FeedComparator implements Comparable<DataModel>, Comparator<DataModel> {

    public int compare(DataModel feed, DataModel feeds) {
        //In the following line you set the criterion,
        //which is the name of Contact in my example scenario
        return feed.getFrom_user_name().compareTo(feeds.getFrom_user_name());
    }

    @Override
    public Comparator<DataModel> reversed() {
        return null;
    }

    @Override
    public int compareTo(@NonNull DataModel t) {
        return 0;
    }
}
