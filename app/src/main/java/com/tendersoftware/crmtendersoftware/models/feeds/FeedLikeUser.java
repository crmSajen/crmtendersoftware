package com.tendersoftware.crmtendersoftware.models.feeds;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 11/30/2017.
 */

public class FeedLikeUser {

    @SerializedName("user_id")
    @Expose
    private Integer user_id;
    @SerializedName("user_name")
    @Expose
    private String user_name;
    @SerializedName("user_likedon")
    @Expose
    private String user_likedon;
    @SerializedName("user_liked_date")
    @Expose
    private String user_liked_date;

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_likedon() {
        return user_likedon;
    }

    public void setUser_likedon(String user_likedon) {
        this.user_likedon = user_likedon;
    }

    public String getUser_liked_date() {
        return user_liked_date;
    }

    public void setUser_liked_date(String user_liked_date) {
        this.user_liked_date = user_liked_date;
    }
}
