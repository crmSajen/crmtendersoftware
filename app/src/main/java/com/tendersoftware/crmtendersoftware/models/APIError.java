package com.tendersoftware.crmtendersoftware.models;

/**
 * Created by SYS on 11/23/2017.
 */

public class APIError {

    private int statusCode;
    private String message;

    public APIError() {
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }

}
