package com.tendersoftware.crmtendersoftware.models.feeds;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SYS on 9/21/2017.
 */

public class ResultModel {

    @SerializedName("data")
    @Expose
    private List<DataModel> data = null;
    @SerializedName("parentdata")
    @Expose
    private String parentdata;
    @SerializedName("tot_records")
    @Expose
    private Integer tot_records;
    @SerializedName("tot_found")
    @Expose
    private Integer tot_found;
    @SerializedName("last_date")
    @Expose
    private Integer last_date;

    public List<DataModel> getData() {
        return data;
    }

    public void setData(List<DataModel> data) {
        this.data = data;
    }

    public String getParentdata() {
        return parentdata;
    }

    public void setParentdata(String parentdata) {
        this.parentdata = parentdata;
    }

    public Integer getTot_records() {
        return tot_records;
    }

    public void setTot_records(Integer tot_records) {
        this.tot_records = tot_records;
    }

    public Integer getTot_found() {
        return tot_found;
    }

    public void setTot_found(Integer tot_found) {
        this.tot_found = tot_found;
    }

    public Integer getLast_date() {
        return last_date;
    }

    public void setLast_date(Integer last_date) {
        this.last_date = last_date;
    }
}
