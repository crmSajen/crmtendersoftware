package com.tendersoftware.crmtendersoftware.models.feeds;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 9/21/2017.
 */

public class LikeDataModel {

    @SerializedName("noteid")
    @Expose
    private String noteid;
    @SerializedName("likecount")
    @Expose
    private Integer likecount;
    @SerializedName("stafflike_post_label")
    @Expose
    private String stafflike_post_label;
    @SerializedName("stafflike_post")
    @Expose
    private Integer stafflike_post;

    public String getNoteid() {
        return noteid;
    }

    public void setNoteid(String noteid) {
        this.noteid = noteid;
    }

    public Integer getLikecount() {
        return likecount;
    }

    public void setLikecount(Integer likecount) {
        this.likecount = likecount;
    }

    public String getStafflike_post_label() {
        return stafflike_post_label;
    }

    public void setStafflike_post_label(String stafflike_post_label) {
        this.stafflike_post_label = stafflike_post_label;
    }

    public Integer getStafflike_post() {
        return stafflike_post;
    }

    public void setStafflike_post(Integer stafflike_post) {
        this.stafflike_post = stafflike_post;
    }
}
