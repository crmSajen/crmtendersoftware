package com.tendersoftware.crmtendersoftware.models.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Priya on 11/24/2017.
 */

public class CommentRequestModel {

@SerializedName("muid")
@Expose
String muid;
    @SerializedName("id")
    @Expose
    Integer id;
    @SerializedName("module")
    @Expose
    String module;
    @SerializedName("comment")
    @Expose
    String comment;

    public String getMuid() {
        return muid;
    }

    public void setMuid(String muid) {
        this.muid = muid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
