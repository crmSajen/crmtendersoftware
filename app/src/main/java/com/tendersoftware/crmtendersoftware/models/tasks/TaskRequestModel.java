package com.tendersoftware.crmtendersoftware.models.tasks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 11/17/2017.
 */

public class TaskRequestModel {


    @SerializedName("muid")
    @Expose
    private String muid;
    @SerializedName("start")
    @Expose
    private Integer start;
    @SerializedName("length")
    @Expose
    private Integer length;

    public String getMuid() {
        return muid;
    }

    public void setMuid(String muid) {
        this.muid = muid;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }


}
