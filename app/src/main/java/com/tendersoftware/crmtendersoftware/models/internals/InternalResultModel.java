package com.tendersoftware.crmtendersoftware.models.internals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tendersoftware.crmtendersoftware.models.tasks.TaskDataModel;

import java.util.List;

/**
 * Created by SYS on 11/20/2017.
 */

public class InternalResultModel {

    @SerializedName("data")
    @Expose
    private List<InternalDataModel> data = null;

    public List<InternalDataModel> getData() {
        return data;
    }

    public void setData(List<InternalDataModel> data) {
        this.data = data;
    }
}
