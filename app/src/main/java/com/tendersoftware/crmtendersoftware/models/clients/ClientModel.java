package com.tendersoftware.crmtendersoftware.models.clients;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tendersoftware.crmtendersoftware.models.leads.ResultModel;

/**
 * Created by SYS on 11/24/2017.
 */

public class ClientModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("result")
    @Expose
    private ClientResultModel result;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ClientResultModel getResult() {
        return result;
    }

    public void setResult(ClientResultModel result) {
        this.result = result;
    }
}
