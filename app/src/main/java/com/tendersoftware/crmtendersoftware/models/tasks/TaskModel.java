package com.tendersoftware.crmtendersoftware.models.tasks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 11/20/2017.
 */

public class TaskModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("result")
    @Expose
    private TaskResultModel result;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public TaskResultModel getResult() {
        return result;
    }

    public void setResult(TaskResultModel result) {
        this.result = result;
    }
}
