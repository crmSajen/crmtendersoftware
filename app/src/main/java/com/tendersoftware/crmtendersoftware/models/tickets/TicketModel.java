package com.tendersoftware.crmtendersoftware.models.tickets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tendersoftware.crmtendersoftware.models.internals.InternalResultModel;

/**
 * Created by SYS on 11/20/2017.
 */

public class TicketModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("result")
    @Expose
    private TicketResultModel result;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public TicketResultModel getResult() {
        return result;
    }

    public void setResult(TicketResultModel result) {
        this.result = result;
    }
}
