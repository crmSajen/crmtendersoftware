package com.tendersoftware.crmtendersoftware.models.feeds;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SYS on 11/17/2017.
 */
public class FeedsRequestModel {
    @SerializedName("muid")
    @Expose
    private String muid;
    @SerializedName("limit")
    @Expose
    private Integer limit;
    @SerializedName("start")
    @Expose
    private Integer start;
    public String getMuid() {
        return muid;
    }

    public void setMuid(String muid) {
        this.muid = muid;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }
}
