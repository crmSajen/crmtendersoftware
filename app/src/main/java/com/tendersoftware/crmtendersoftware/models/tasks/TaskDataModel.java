package com.tendersoftware.crmtendersoftware.models.tasks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SYS on 11/20/2017.
 */

public class TaskDataModel {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("addedfor")
    @Expose
    private String addedfor;

    @SerializedName("addedfrom")
    @Expose
    private String addedfrom;
    @SerializedName("starttime")
    @Expose
    private String starttime;
    @SerializedName("notifytime")
    @Expose
    private String notifytime;
    @SerializedName("dateadded")
    @Expose
    private String dateadded;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddedfor() {
        return addedfor;
    }

    public void setAddedfor(String addedfor) {
        this.addedfor = addedfor;
    }

    public String getAddedfrom() {
        return addedfrom;
    }

    public void setAddedfrom(String addedfrom) {
        this.addedfrom = addedfrom;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getNotifytime() {
        return notifytime;
    }

    public void setNotifytime(String notifytime) {
        this.notifytime = notifytime;
    }

    public String getDateadded() {
        return dateadded;
    }

    public void setDateadded(String dateadded) {
        this.dateadded = dateadded;
    }
}
