package com.tendersoftware.crmtendersoftware.models.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SYS on 11/24/2017.
 */

public class CommentDataModel {




    @SerializedName("noteDetails")
    @Expose
    private CommentNoteDetails noteDetails;

    @SerializedName("comments")
    @Expose
    private   List<String> comments;

    @SerializedName("likelist")
    @Expose
    private List<String> likelist;
    @SerializedName("attachments")
    @Expose
    private List<String> attachments;
//    @SerializedName("email_threads")
//    @Expose
//    private String email_threads;
    @SerializedName("ticketdetails")
    @Expose
    private List<String> ticketdetails;
    @SerializedName("totCmt")
    @Expose
    private Integer totCmt;

    public CommentNoteDetails getNoteDetails() {
        return noteDetails;
    }

    public void setNoteDetails(CommentNoteDetails noteDetails) {
        this.noteDetails = noteDetails;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public List<String> getLikelist() {
        return likelist;
    }

    public void setLikelist(List<String> likelist) {
        this.likelist = likelist;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }

    public List<String> getTicketdetails() {
        return ticketdetails;
    }

    public void setTicketdetails(List<String> ticketdetails) {
        this.ticketdetails = ticketdetails;
    }

//    public String getEmail_threads() {
//        return email_threads;
//    }
//
//    public void setEmail_threads(String email_threads) {
//        this.email_threads = email_threads;
//    }



    public Integer getTotCmt() {
        return totCmt;
    }

    public void setTotCmt(Integer totCmt) {
        this.totCmt = totCmt;
    }
}
