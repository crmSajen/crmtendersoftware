package com.tendersoftware.crmtendersoftware.models.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tendersoftware.crmtendersoftware.models.clients.ClientResultModel;

/**
 * Created by SYS on 11/24/2017.
 */

public class CommentGetResponseModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("result")
    @Expose
    private CommentResultModel result;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public CommentResultModel getResult() {
        return result;
    }

    public void setResult(CommentResultModel result) {
        this.result = result;
    }
}
