package com.tendersoftware.crmtendersoftware.models.leads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by PadmaPriya on 11/15/2017.
 */

public class LeadRequestModel {

    @SerializedName("muid")
    private String muid;
    @SerializedName("start")
    private Integer start;
    @SerializedName("length")
    private Integer length;

    public String getMuid() {
        return muid;
    }

    public void setMuid(String muid) {
        this.muid = muid;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }
}
