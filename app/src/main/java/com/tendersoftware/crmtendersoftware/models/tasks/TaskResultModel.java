package com.tendersoftware.crmtendersoftware.models.tasks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tendersoftware.crmtendersoftware.models.feeds.DataModel;

import java.util.List;

/**
 * Created by SYS on 11/20/2017.
 */

public class TaskResultModel {

    @SerializedName("data")
    @Expose
    private List<TaskDataModel> data = null;

    public List<TaskDataModel> getData() {
        return data;
    }

    public void setData(List<TaskDataModel> data) {
        this.data = data;
    }
}
