package com.tendersoftware.crmtendersoftware.models.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tendersoftware.crmtendersoftware.models.clients.ClientDataModel;

import java.util.List;

/**
 * Created by SYS on 11/24/2017.
 */

public class CommentResultModel {

    @SerializedName("data")
    @Expose
    private List<CommentDataModel> data = null;

    public List<CommentDataModel> getData() {
        return data;
    }

    public void setData(List<CommentDataModel> data) {
        this.data = data;
    }

}
