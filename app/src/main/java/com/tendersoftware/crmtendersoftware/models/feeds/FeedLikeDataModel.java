package com.tendersoftware.crmtendersoftware.models.feeds;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SYS on 11/30/2017.
 */

public class FeedLikeDataModel {

    @SerializedName("noteid")
    @Expose
    private Integer noteid;
    @SerializedName("likecount")
    @Expose
    private Integer likecount;
    @SerializedName("stafflike_post_label")
    @Expose
    private String stafflike_post_label;
    @SerializedName("stafflike_post")
    @Expose
    private String stafflike_post;
    @SerializedName("likeusers")
    @Expose
    private List<FeedLikeUser> likeusers;

    public Integer getNoteid() {
        return noteid;
    }

    public void setNoteid(Integer noteid) {
        this.noteid = noteid;
    }

    public Integer getLikecount() {
        return likecount;
    }

    public void setLikecount(Integer likecount) {
        this.likecount = likecount;
    }

    public String getStafflike_post_label() {
        return stafflike_post_label;
    }

    public void setStafflike_post_label(String stafflike_post_label) {
        this.stafflike_post_label = stafflike_post_label;
    }

    public String getStafflike_post() {
        return stafflike_post;
    }

    public void setStafflike_post(String stafflike_post) {
        this.stafflike_post = stafflike_post;
    }

    public List<FeedLikeUser> getLikeusers() {
        return likeusers;
    }

    public void setLikeusers(List<FeedLikeUser> likeusers) {
        this.likeusers = likeusers;
    }
}
