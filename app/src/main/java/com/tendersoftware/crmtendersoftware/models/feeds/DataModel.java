package com.tendersoftware.crmtendersoftware.models.feeds;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SYS on 9/21/2017.
 */

public class DataModel implements Comparable<DataModel>  {

    @SerializedName("to_userid")
    @Expose
    private String to_userid;
    @SerializedName("from_userid")
    @Expose
    private String from_userid;
    @SerializedName("from_user_name")
    @Expose
    private String from_user_name;
    @SerializedName("from_user_image")
    @Expose
    private String from_user_image;

    @SerializedName("isLikeVisible")
    @Expose
    private Boolean isLikeVisible;
    @SerializedName("isTicketVisible")
    @Expose
    private Boolean isTicketVisible;
    @SerializedName("isTaskVisible")
    @Expose
    private Boolean isTaskVisible;
    @SerializedName("isTicketed")
    @Expose
    private Integer isTicketed;
    @SerializedName("dataid")
    @Expose
    private String dataid;
    @SerializedName("datatype")
    @Expose
    private String datatype;
    @SerializedName("title_des")
    @Expose
    private String title_des;
    @SerializedName("feed_date")
    @Expose
    private String feed_date;
    @SerializedName("timeago")
    @Expose
    private String timeago;
    @SerializedName("reply")
    @Expose
    private List<ReplyModel> reply = null;
    @SerializedName("parentDiv")
    @Expose
    private String parentDiv;
    @SerializedName("feed_id")
    @Expose
    private Integer feed_id;
    @SerializedName("feed_des")
    @Expose
    private String feed_des;
    @SerializedName("like_data")
    @Expose
    private FeedLikeDataModel like_data;


    public String getTo_userid() {
        return to_userid;
    }

    public void setTo_userid(String to_userid) {
        this.to_userid = to_userid;
    }

    public String getFrom_userid() {
        return from_userid;
    }

    public void setFrom_userid(String from_userid) {
        this.from_userid = from_userid;
    }

    public String getFrom_user_name() {
        return from_user_name;
    }

    public void setFrom_user_name(String from_user_name) {
        this.from_user_name = from_user_name;
    }

    public String getFrom_user_image() {
        return from_user_image;
    }

    public void setFrom_user_image(String from_user_image) {
        this.from_user_image = from_user_image;
    }

    public String getDataid() {
        return dataid;
    }

    public void setDataid(String dataid) {
        this.dataid = dataid;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public String getTitle_des() {
        return title_des;
    }

    public void setTitle_des(String title_des) {
        this.title_des = title_des;
    }

    public String getFeed_date() {
        return feed_date;
    }

    public void setFeed_date(String feed_date) {
        this.feed_date = feed_date;
    }

    public String getTimeago() {
        return timeago;
    }

    public void setTimeago(String timeago) {
        this.timeago = timeago;
    }

    public List<ReplyModel> getReply() {
        return reply;
    }

    public void setReply(List<ReplyModel> reply) {
        this.reply = reply;
    }

    public String getParentDiv() {
        return parentDiv;
    }

    public void setParentDiv(String parentDiv) {
        this.parentDiv = parentDiv;
    }

    public Integer getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(Integer feed_id) {
        this.feed_id = feed_id;
    }

    public String getFeed_des() {
        return feed_des;
    }

    public void setFeed_des(String feed_des) {
        this.feed_des = feed_des;
    }

    public Boolean getLikeVisible() {
        return isLikeVisible;
    }

    public void setLikeVisible(Boolean likeVisible) {
        isLikeVisible = likeVisible;
    }

    public Boolean getTicketVisible() {
        return isTicketVisible;
    }

    public void setTicketVisible(Boolean ticketVisible) {
        isTicketVisible = ticketVisible;
    }

    public Boolean getTaskVisible() {
        return isTaskVisible;
    }

    public void setTaskVisible(Boolean taskVisible) {
        isTaskVisible = taskVisible;
    }

    public Integer getIsTicketed() {
        return isTicketed;
    }

    public void setIsTicketed(Integer isTicketed) {
        this.isTicketed = isTicketed;
    }

    public FeedLikeDataModel getLike_data() {
        return like_data;
    }

    public void setLike_data(FeedLikeDataModel like_data) {
        this.like_data = like_data;
    }

    @Override
    public int compareTo(@NonNull DataModel dataModel) {
        return getFrom_user_name().compareTo(dataModel.getFrom_user_name());
    }
}
