package com.tendersoftware.crmtendersoftware.callrecord;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.tendersoftware.crmtendersoftware.MainActivity;
import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.utils.Utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main2Activity extends Activity {

    private static final int REQUEST_CODE = 0;
    private static final String TAG = "";
    private DevicePolicyManager mDPM;
    private ComponentName mAdminName;
    private boolean permissionToRecordAccepted = false;
    private boolean permissionToWriteAccepted = false;
    private String [] permissions = {"android.permission.RECORD_AUDIO", "android.permission.WRITE_EXTERNAL_STORAGE"};
    private String mPhoneNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        int requestCode = 200;
        Log.d(TAG,"result code234 "+requestCode);
        Intent in = getIntent();
        Bundle b = in.getExtras();
    mPhoneNumber = b.getString("phone_number");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d(TAG,"result code345 "+requestCode);

            requestPermissions(permissions, requestCode);
        }else {
            Utilities.getInstance().makeCall(this,"tel:"+String.valueOf(mPhoneNumber));

        }
//        try {
//            // Initiate DevicePolicyManager.
//            mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
//            mAdminName = new ComponentName(this, DeviceAdminDemo.class);
//
//            if (!mDPM.isAdminActive(mAdminName)) {
//                Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
//                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
//                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Click on Activate button to secure your application.");
//                startActivityForResult(intent, REQUEST_CODE);
//            } else {
//                // mDPM.lockNow();
//                // Intent intent = new Intent(MainActivity.this,
//                // TrackDeviceService.class);
//                // startService(intent);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

//        Utilities.getInstance().makeCall(this,"tel:"+"9567930150");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"result code "+requestCode);
        if (REQUEST_CODE == requestCode) {
            Intent intent = new Intent(Main2Activity.this, TService.class);
            startService(intent);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG,"result code123 "+requestCode);

        switch (requestCode){

            case 200:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                permissionToWriteAccepted  = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                break;
        }
        if (!permissionToRecordAccepted || !permissionToWriteAccepted ) Main2Activity.super.finish();
       else  {Utilities.getInstance().makeCall(this,"tel:"+String.valueOf(mPhoneNumber));
            Main2Activity.super.finish();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(this.getIntent().getExtras().getInt("kill")==1) {
            Log.d(TAG,"result kill ");
            Main2Activity.super.finish();
        }
    }

   }
