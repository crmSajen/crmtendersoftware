package com.tendersoftware.crmtendersoftware.callrecord;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tendersoftware.crmtendersoftware.MainActivity;
import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.adapter.CallRecordAdapter;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.fragments.BaseFragment;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by SYS on 11/17/2017.
 */

public class CallRecordedList extends BaseFragment {


    private static final String TAG = "";
    LoadingView loadingView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<String> mRecordedList = new ArrayList<String>();
    CallRecordAdapter mCallRecordAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.call_recordedlist,container,false);
        ButterKnife.bind(this,view);
        uiSetup();

        return view;
    }

    private void uiSetup() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        getRecordeFileList();
        swipeRefreshLayout.setColorSchemeResources(R.color.app_color);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void getRecordeFileList() {
        File file=new File(Environment.getExternalStorageDirectory(), "/TendersoftRecord");
        File[] list = file.listFiles();
        for (File f: list){
            String name = f.getName();
//            if (name.endsWith(".jpg") || name.endsWith(".mp3") || name.endsWith(".some media extention"))
            System.out.println("test files " + name);
            mRecordedList.add(name);
        }

        mCallRecordAdapter = new CallRecordAdapter(mRecordedList,getActivity());
        recyclerView.setAdapter(mCallRecordAdapter);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode){
//            case 200:
//                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                permissionToWriteAccepted  = grantResults[1] == PackageManager.PERMISSION_GRANTED;
//                break;
//        }
//        if (!permissionToRecordAccepted ) MainActivity.super.finish();
//        if (!permissionToWriteAccepted ) MainActivity.super.finish();
//
//    }

}
