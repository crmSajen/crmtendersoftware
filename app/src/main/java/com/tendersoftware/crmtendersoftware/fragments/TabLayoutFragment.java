package com.tendersoftware.crmtendersoftware.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.adapter.ViewPagerAdapter;
import com.tendersoftware.crmtendersoftware.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by SYS on 9/20/2017.
 */

public class TabLayoutFragment extends BaseFragment {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    String toComponent = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!= null)
            toComponent = bundle.getString("TO");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_layout, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        if(!toComponent.equals("")) {
            if (toComponent.equals("LEADS"))
                adapter.addFragment(new LeadsFragment(), "RECENT");
            else if (toComponent.equals("CLIENTS"))
                adapter.addFragment(new ClientFragment(), "CLIENTS");
            else if (toComponent.equals("FEEDS"))
                adapter.addFragment(new FeedFragment(), "FEEDS");
            else if (toComponent.equals("INTERNAL"))
                adapter.addFragment(new Internalfragment(), "DISCUSSIONS");
            else if (toComponent.equals("TASKS"))
                adapter.addFragment(new TasksFragment(), "REMINDERS");
            else if (toComponent.equals("TICKET"))
                adapter.addFragment(new TicketFragment(), "TICKETS");
        }
//        adapter.addFragment(new SalesUnit(), "ALPHABETICAL");
        adapter.addFragment(new ConstructionFragment(), "ALPHABETICAL");

        viewPager.setAdapter(adapter);
    }

}
