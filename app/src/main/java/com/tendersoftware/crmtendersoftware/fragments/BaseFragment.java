package com.tendersoftware.crmtendersoftware.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.tendersoftware.crmtendersoftware.MainActivity;

public abstract class BaseFragment extends Fragment {

    public MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (MainActivity) this.getActivity();

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

}