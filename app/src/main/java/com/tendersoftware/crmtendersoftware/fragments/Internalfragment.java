package com.tendersoftware.crmtendersoftware.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.activities.CommentActivity;
import com.tendersoftware.crmtendersoftware.activities.LoginActivity;
import com.tendersoftware.crmtendersoftware.adapter.InternalAdapter;
import com.tendersoftware.crmtendersoftware.adapter.LeadsAdapter;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.models.internals.InternalDataModel;
import com.tendersoftware.crmtendersoftware.models.internals.InternalModel;
import com.tendersoftware.crmtendersoftware.models.internals.InternalRequestModel;
import com.tendersoftware.crmtendersoftware.models.internals.InternalResultModel;
import com.tendersoftware.crmtendersoftware.models.leads.DataModel;
import com.tendersoftware.crmtendersoftware.session.CRMSessionConstants;
import com.tendersoftware.crmtendersoftware.session.CRMSharedPreference;
import com.tendersoftware.crmtendersoftware.utils.Utilities;
import com.tendersoftware.crmtendersoftware.webservice.WebService;
import com.tendersoftware.crmtendersoftware.webservice.interfaces.WebServiceAPI;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by SYS on 10/3/2017.
 */

public class Internalfragment extends BaseFragment implements LeadsAdapter.OnItemClickListener {

    LoadingView loadingView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private InternalAdapter internalAdapter;
    List<InternalDataModel> data = new ArrayList<>();
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout mCoordinatorLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.leads_layout, container, false);
        ButterKnife.bind(this, view);
        uiSetup();
        return view;
    }

    private void uiSetup() {
        internalAdapter = new InternalAdapter(data, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(internalAdapter);

        swipeRefreshLayout.setColorSchemeResources(R.color.app_color);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utilities.getInstance().isNetworkAvailable(getActivity()))
                    getInternalDetails();
                else
                    Utilities.getInstance().showSnackBarShort(getActivity(), getString(R.string.networkstatus),mCoordinatorLayout);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (Utilities.getInstance().isNetworkAvailable(getActivity()))
            getInternalDetails();
        else
            Utilities.getInstance().showSnackBarShort(getActivity(),getString(R.string.networkstatus),mCoordinatorLayout);
    }

    private void getInternalDetails() {
        loadingView = Utilities.loadingView(getActivity(), "Loading");
        InternalRequestModel mInternalRequestModel = new InternalRequestModel();
        mInternalRequestModel.setMuid(CRMSharedPreference.getInstance(getActivity()).getString(CRMSessionConstants.UID));
        mInternalRequestModel.setStart(0);
        mInternalRequestModel.setLength(2);

        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        Call<InternalModel> responseBodyCall = webServiceAPI.internals(mInternalRequestModel);
        responseBodyCall.enqueue(new Callback<InternalModel>() {
            @Override
            public void onResponse(Call<InternalModel> call, Response<InternalModel> response) {
                loadingView.dismiss();
                if (response.body().getSuccess()) {
                    data.clear();
                    data.addAll(response.body().getResult().getData());

                    Log.d(TAG,"Response Data ::::::: "+response.isSuccessful()+" "+new Gson().toJson(response.body()));

                    internalAdapter.notifyDataSetChanged();

//                    Bundle b = new Bundle();
//                    b.putString("comment", "internal");
//                    b.putInt("lead_id",data.get(0).getId());
//                    Intent in = new Intent(getActivity(), CommentActivity.class);
//                    in.putExtras(b);
//
//                    startActivity(in);

                    if (data.size() == 0)
                        Utilities.getInstance().showSnackBarShort(getActivity(), "No items found",mCoordinatorLayout);

                } else {
                    Utilities.getInstance().alertDialogSingleOptions(getActivity(), getString(R.string.invalid_user));
                }
            }

            @Override
            public void onFailure(Call<InternalModel> call, Throwable t) {
                loadingView.dismiss();
                Utilities.getInstance().showSnackBarShort(getActivity(), t.getMessage(),mCoordinatorLayout);
            }
        });

        Utilities.getInstance().closeKeyboard(getActivity());
    }

    @Override
    public void onItemClick(View view, String val) {

    }
}

