package com.tendersoftware.crmtendersoftware.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.adapter.FeedAlphabeticAdapter;
import com.tendersoftware.crmtendersoftware.adapter.FeedsAdapter;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.models.feeds.DataModel;
import com.tendersoftware.crmtendersoftware.models.feeds.FeedComparator;
import com.tendersoftware.crmtendersoftware.models.feeds.FeedsModel;
import com.tendersoftware.crmtendersoftware.models.feeds.FeedsRequestModel;
import com.tendersoftware.crmtendersoftware.session.CRMSessionConstants;
import com.tendersoftware.crmtendersoftware.session.CRMSharedPreference;
import com.tendersoftware.crmtendersoftware.utils.NameComparator;
import com.tendersoftware.crmtendersoftware.utils.Utilities;
import com.tendersoftware.crmtendersoftware.webservice.WebService;
import com.tendersoftware.crmtendersoftware.webservice.interfaces.WebServiceAPI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SalesUnit  extends BaseFragment  {

    private static final String TAG = "";
    LoadingView loadingView;
    private FeedAlphabeticAdapter feedsAdapter;
    List<DataModel> data = new ArrayList<>();
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout mCoordinatorLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.getInstance().closeKeyboard(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.leads_layout,container,false);
        ButterKnife.bind(this,view);
        uiSetup();
        return view;
    }

    private void uiSetup(){
        feedsAdapter = new FeedAlphabeticAdapter(data,getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(feedsAdapter);

        swipeRefreshLayout.setColorSchemeResources(R.color.app_color);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {


                if (Utilities.getInstance().isNetworkAvailable(getActivity()))
                    getFeedDetails();
                else
                    Utilities.getInstance().showSnackBarShort(getActivity(), getString(R.string.networkstatus),mCoordinatorLayout);

                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (Utilities.getInstance().isNetworkAvailable(getActivity()))
            getFeedDetails();
        else
            Utilities.getInstance().showSnackBarShort(getActivity(),getString(R.string.networkstatus),mCoordinatorLayout);
    }

    private void getFeedDetails(){
        loadingView =   Utilities.loadingView(getActivity(),"Loading");
        data.clear();


        FeedsRequestModel mFeedsRequestModel = new FeedsRequestModel();
        mFeedsRequestModel.setMuid(CRMSharedPreference.getInstance(getActivity()).getString(CRMSessionConstants.UID));
        mFeedsRequestModel.setLimit(10);
        mFeedsRequestModel.setStart(10);

        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        Call<FeedsModel> responseBodyCall = webServiceAPI.feeds(mFeedsRequestModel);
        responseBodyCall.enqueue(new Callback<FeedsModel>() {
            @Override
            public void onResponse(Call<FeedsModel> call, Response<FeedsModel> response) {
                loadingView.dismiss();
                if(response.body().getSuccess()){
                    data.addAll(response.body().getResult().getData());
//                    Collections.sort(data, new FeedComparator());
                    Collections.sort(data);

                    feedsAdapter.notifyDataSetChanged();
                    if (data.size() == 0)
                        Utilities.getInstance().showSnackBarShort(getActivity(), "No items found",mCoordinatorLayout);


                }
                else {
                    Utilities.getInstance().alertDialogSingleOptions(getActivity(), getString(R.string.invalid_user));



                }
            }
            @Override
            public void onFailure(Call<FeedsModel> call, Throwable t) {
                loadingView.dismiss();
                t.printStackTrace();
//                Toast.makeText(mainActivity, "FEED FRAGMENT Error", Toast.LENGTH_SHORT).show();
                Utilities.getInstance().showSnackBarShort(getActivity(), t.getMessage(),mCoordinatorLayout);
            }
        });
    }

}
