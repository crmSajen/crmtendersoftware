package com.tendersoftware.crmtendersoftware.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.models.login.LoginModel;
import com.tendersoftware.crmtendersoftware.utils.Utilities;
import com.tendersoftware.crmtendersoftware.webservice.WebService;
import com.tendersoftware.crmtendersoftware.webservice.interfaces.WebServiceAPI;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SYS on 9/21/2017.
 */

public class ForgotPasswordDialog extends DialogFragment {

    @BindView(R.id.email_edittext)
    EditText emailEditText;
    @BindView(R.id.forgot_layout)
    LinearLayout forgotLayout;
    LoadingView loadingView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forgot_password,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @OnClick(R.id.submit_button)
    public void submit(){
        if(!Utilities.getInstance().isValidEmail(emailEditText.getText().toString())) {
            Utilities.getInstance().showSnackBarShort(getActivity(), "Please enter valid Email-Id", forgotLayout);
        }
        else {
            if (Utilities.getInstance().isNetworkAvailable(getActivity()))
                sendForgetPassword();
            else
                Utilities.getInstance().showToast(getActivity(),R.string.networkstatus);
        }
    }

    @OnClick(R.id.btnCancel)
    public void cancel(){
        getDialog().dismiss();
    }

    private void sendForgetPassword(){
        loadingView =   Utilities.loadingView(getActivity(),"Loading");
        Map<String, Object> map = new HashMap<>();
        map.put("email",emailEditText.getText().toString());
        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        Call<LoginModel> responseBodyCall = webServiceAPI.forgetPassword(emailEditText.getText().toString());
        responseBodyCall.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                loadingView.dismiss();
                if(response.body().getSuccess()) {
                    Utilities.getInstance().showSnackBarShort(getActivity(), "Email Sent to you mail id", forgotLayout);
                    getDialog().dismiss();
                }
                else
                    Utilities.getInstance().showSnackBarShort(getActivity(), response.body().getError().getMessage(),forgotLayout);
            }
            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                loadingView.dismiss();
                Utilities.getInstance().showSnackBarShort(getActivity(), "Something went wrong",forgotLayout);
            }
        });
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }
}
