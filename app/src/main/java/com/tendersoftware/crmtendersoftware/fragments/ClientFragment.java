package com.tendersoftware.crmtendersoftware.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.activities.CommentActivity;
import com.tendersoftware.crmtendersoftware.adapter.ClientAdapter;
import com.tendersoftware.crmtendersoftware.adapter.LeadsAdapter;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.models.clients.ClientDataModel;
import com.tendersoftware.crmtendersoftware.models.clients.ClientModel;
import com.tendersoftware.crmtendersoftware.models.clients.ClientRequestModel;
import com.tendersoftware.crmtendersoftware.models.leads.DataModel;
import com.tendersoftware.crmtendersoftware.models.leads.LeadsModel;
import com.tendersoftware.crmtendersoftware.session.CRMSessionConstants;
import com.tendersoftware.crmtendersoftware.session.CRMSharedPreference;
import com.tendersoftware.crmtendersoftware.utils.NameComparator;
import com.tendersoftware.crmtendersoftware.utils.Utilities;
import com.tendersoftware.crmtendersoftware.webservice.WebService;
import com.tendersoftware.crmtendersoftware.webservice.interfaces.WebServiceAPI;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SYS on 9/20/2017.
 */

public class ClientFragment extends BaseFragment {

    LoadingView  loadingView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private ClientAdapter mClientAdapter;
    List<ClientDataModel> data = new ArrayList<>();
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout mCoordinatorLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.getInstance().closeKeyboard(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.leads_layout,container,false);
        ButterKnife.bind(this,view);
        uiSetup();
        return view;
    }

    private void uiSetup(){
        mClientAdapter = new ClientAdapter(data,getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        mClientAdapter.setOnItemClickListener(new LeadsAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View v,String val) {
//                Collections.sort(data, new NameComparator(val));
//                mClientAdapter.notifyDataSetChanged();
//            }
//        });
        recyclerView.setAdapter(mClientAdapter);

        swipeRefreshLayout.setColorSchemeResources(R.color.app_color);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utilities.getInstance().isNetworkAvailable(getActivity()))
                    getLeadDetails();
                else
                Utilities.getInstance().showSnackBarShort(getActivity(), getString(R.string.networkstatus),mCoordinatorLayout);

                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (Utilities.getInstance().isNetworkAvailable(getActivity()))
            getLeadDetails();
        else
            Utilities.getInstance().showSnackBarShort(getActivity(),getString(R.string.networkstatus),mCoordinatorLayout);


    }

    private void getLeadDetails(){
        loadingView =   Utilities.loadingView(getActivity(),"Loading");
        data.clear();


        ClientRequestModel mClientRequestModel = new ClientRequestModel();
        mClientRequestModel.setMuid(CRMSharedPreference.getInstance(getActivity()).getString(CRMSessionConstants.UID));
        mClientRequestModel.setStart(0);
        mClientRequestModel.setLength(2);
        mClientRequestModel.setSalesUnit(1);
        mClientRequestModel.setStatus(1);

        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        Call<ClientModel> responseBodyCall = webServiceAPI.clients(mClientRequestModel);
        responseBodyCall.enqueue(new Callback<ClientModel>() {
            @Override
            public void onResponse(Call<ClientModel> call, Response<ClientModel> response) {
                loadingView.dismiss();
                if(response.body().getSuccess()){
                    data.addAll(response.body().getResult().getData());
                    mClientAdapter.notifyDataSetChanged();



                    if (data.size() == 0)
                        Utilities.getInstance().showSnackBarShort(getActivity(), "No items found",mCoordinatorLayout);
                }
                else {
                    Utilities.getInstance().alertDialogSingleOptions(getActivity(), getString(R.string.invalid_user));

                }
            }
            @Override
            public void onFailure(Call<ClientModel> call, Throwable t) {
                loadingView.dismiss();
                Utilities.getInstance().showSnackBarShort(getActivity(), t.getMessage(),mCoordinatorLayout);
            }
        });
    }
}
