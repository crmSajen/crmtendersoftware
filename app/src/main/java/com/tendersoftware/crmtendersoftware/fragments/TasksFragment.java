package com.tendersoftware.crmtendersoftware.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.activities.LoginActivity;
import com.tendersoftware.crmtendersoftware.adapter.LeadsAdapter;
import com.tendersoftware.crmtendersoftware.adapter.TaskAdapter;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.models.leads.DataModel;
import com.tendersoftware.crmtendersoftware.models.leads.LeadRequestModel;
import com.tendersoftware.crmtendersoftware.models.leads.LeadsModel;
import com.tendersoftware.crmtendersoftware.models.tasks.TaskDataModel;
import com.tendersoftware.crmtendersoftware.models.tasks.TaskModel;
import com.tendersoftware.crmtendersoftware.models.tasks.TaskRequestModel;
import com.tendersoftware.crmtendersoftware.session.CRMSessionConstants;
import com.tendersoftware.crmtendersoftware.session.CRMSharedPreference;
import com.tendersoftware.crmtendersoftware.utils.Utilities;
import com.tendersoftware.crmtendersoftware.webservice.WebService;
import com.tendersoftware.crmtendersoftware.webservice.interfaces.WebServiceAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SYS on 10/3/2017.
 */

public class TasksFragment extends BaseFragment implements LeadsAdapter.OnItemClickListener {

    LoadingView loadingView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private TaskAdapter taskAdapter;
    List<TaskDataModel> data = new ArrayList<>();
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout mCoordinatorLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.leads_layout, container, false);
        ButterKnife.bind(this, view);
        uiSetup();
        return view;
    }

    private void uiSetup() {
        taskAdapter = new TaskAdapter(data, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(taskAdapter);

        swipeRefreshLayout.setColorSchemeResources(R.color.app_color);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utilities.getInstance().isNetworkAvailable(getActivity()))
                    getTaskDetails();
                else
                    Utilities.getInstance().showSnackBarShort(getActivity(),getString(R.string.networkstatus),mCoordinatorLayout);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (Utilities.getInstance().isNetworkAvailable(getActivity()))
            getTaskDetails();
        else
            Utilities.getInstance().showSnackBarShort(getActivity(),getString(R.string.networkstatus),mCoordinatorLayout);
    }

    private void getTaskDetails() {
        loadingView = Utilities.loadingView(getActivity(), "Loading");


        TaskRequestModel mTaskRequestModel = new TaskRequestModel();
        mTaskRequestModel.setMuid(CRMSharedPreference.getInstance(getActivity()).getString(CRMSessionConstants.UID));
        mTaskRequestModel.setStart(0);
        mTaskRequestModel.setLength(2);


        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        Call<TaskModel> responseBodyCall = webServiceAPI.tasks(mTaskRequestModel);

        responseBodyCall.enqueue(new Callback<TaskModel>() {
            @Override
            public void onResponse(Call<TaskModel> call, Response<TaskModel> response) {
                loadingView.dismiss();
                if (response.body().getSuccess()) {
                    data.clear();
                    data.addAll(response.body().getResult().getData());
                    taskAdapter.notifyDataSetChanged();
                    if (data.size() == 0)
                        Utilities.getInstance().showSnackBarShort(getActivity(), "No items found",mCoordinatorLayout);
                } else {
                    Utilities.getInstance().alertDialogSingleOptions(getActivity(), getString(R.string.invalid_user));
                }
            }

            @Override
            public void onFailure(Call<TaskModel> call, Throwable t) {
                loadingView.dismiss();
                Utilities.getInstance().showSnackBarShort(getActivity(), t.getMessage(),mCoordinatorLayout);
            }
        });

        Utilities.getInstance().closeKeyboard(getActivity());
    }

    @Override
    public void onItemClick(View view, String val) {

    }
}
