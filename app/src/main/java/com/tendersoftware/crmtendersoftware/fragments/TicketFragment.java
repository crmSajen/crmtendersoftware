package com.tendersoftware.crmtendersoftware.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.activities.CommentActivity;
import com.tendersoftware.crmtendersoftware.activities.LoginActivity;
import com.tendersoftware.crmtendersoftware.adapter.LeadsAdapter;
import com.tendersoftware.crmtendersoftware.adapter.TicketAdapter;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.models.leads.DataModel;
import com.tendersoftware.crmtendersoftware.models.leads.LeadRequestModel;
import com.tendersoftware.crmtendersoftware.models.leads.LeadsModel;
import com.tendersoftware.crmtendersoftware.models.tasks.TaskRequestModel;
import com.tendersoftware.crmtendersoftware.models.tickets.TicketDataModel;
import com.tendersoftware.crmtendersoftware.models.tickets.TicketModel;
import com.tendersoftware.crmtendersoftware.models.tickets.TicketRequestModel;
import com.tendersoftware.crmtendersoftware.session.CRMSessionConstants;
import com.tendersoftware.crmtendersoftware.session.CRMSharedPreference;
import com.tendersoftware.crmtendersoftware.utils.Utilities;
import com.tendersoftware.crmtendersoftware.webservice.WebService;
import com.tendersoftware.crmtendersoftware.webservice.interfaces.WebServiceAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SYS on 10/3/2017.
 */

public class TicketFragment extends BaseFragment implements LeadsAdapter.OnItemClickListener {

    LoadingView loadingView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private TicketAdapter taskAdapter;
    List<TicketDataModel> data = new ArrayList<>();
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout mCoordinatorLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.leads_layout, container, false);
        ButterKnife.bind(this, view);
        uiSetup();
        return view;
    }

    private void uiSetup() {
        taskAdapter = new TicketAdapter(data, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(taskAdapter);

        swipeRefreshLayout.setColorSchemeResources(R.color.app_color);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Utilities.getInstance().isNetworkAvailable(getActivity()))
                    getTicketDetails();
                else
                    Utilities.getInstance().showSnackBarShort(getActivity(),getString(R.string.networkstatus),mCoordinatorLayout);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (Utilities.getInstance().isNetworkAvailable(getActivity()))
            getTicketDetails();
        else
            Utilities.getInstance().showSnackBarShort(getActivity(),getString(R.string.networkstatus),mCoordinatorLayout);
    }

    private void getTicketDetails() {
        loadingView = Utilities.loadingView(getActivity(), "Loading");


        TicketRequestModel mTicketRequestModel = new TicketRequestModel();
        mTicketRequestModel.setMuid(CRMSharedPreference.getInstance(getActivity()).getString(CRMSessionConstants.UID));
        mTicketRequestModel.setStart(0);
        mTicketRequestModel.setLength(2);


        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        Call<TicketModel> responseBodyCall = webServiceAPI.tickets(mTicketRequestModel);

        responseBodyCall.enqueue(new Callback<TicketModel>() {
            @Override
            public void onResponse(Call<TicketModel> call, Response<TicketModel> response) {
                loadingView.dismiss();
                if (response.body().getSuccess()) {
                    data.clear();
                    data.addAll(response.body().getResult().getData());
                    taskAdapter.notifyDataSetChanged();

//                    Bundle b = new Bundle();
//                    b.putString("comment", "internal");
//                    b.putInt("lead_id",data.get(0).getTblticketsTicketid());
//                    Intent in = new Intent(getActivity(), CommentActivity.class);
//                    in.putExtras(b);
//
//                    startActivity(in);

                    if (data.size() == 0)
                        Utilities.getInstance().showSnackBarShort(getActivity(), "No items found",mCoordinatorLayout);
                } else {
                    Utilities.getInstance().alertDialogSingleOptions(getActivity(), getString(R.string.invalid_user));
                }
            }

            @Override
            public void onFailure(Call<TicketModel> call, Throwable t) {
                loadingView.dismiss();
                Utilities.getInstance().showSnackBarShort(getActivity(), t.getMessage(),mCoordinatorLayout);
            }
        });

        Utilities.getInstance().closeKeyboard(getActivity());
    }

    @Override
    public void onItemClick(View view, String val) {

    }
}
