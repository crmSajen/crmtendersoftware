package com.tendersoftware.crmtendersoftware.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.activities.CommentActivity;
import com.tendersoftware.crmtendersoftware.activities.LoginActivity;
import com.tendersoftware.crmtendersoftware.adapter.LeadsAdapter;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.models.leads.DataModel;
import com.tendersoftware.crmtendersoftware.models.leads.LeadRequestModel;
import com.tendersoftware.crmtendersoftware.models.leads.LeadsModel;
import com.tendersoftware.crmtendersoftware.session.CRMSessionConstants;
import com.tendersoftware.crmtendersoftware.session.CRMSharedPreference;
import com.tendersoftware.crmtendersoftware.utils.NameComparator;
import com.tendersoftware.crmtendersoftware.utils.Utilities;
import com.tendersoftware.crmtendersoftware.webservice.WebService;
import com.tendersoftware.crmtendersoftware.webservice.interfaces.WebServiceAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by SYS on 9/20/2017.
 */

public class LeadsFragment extends BaseFragment implements LeadsAdapter.OnItemClickListener {

    LoadingView loadingView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private LeadsAdapter leadsAdapter;
    List<DataModel> data = new ArrayList<>();
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout mCoordinatorLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.leads_layout,container,false);
        ButterKnife.bind(this,view);
        uiSetup();
        return view;
    }

    private void uiSetup(){
        leadsAdapter = new LeadsAdapter(data,getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        leadsAdapter.setOnItemClickListener(new LeadsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v,String val) {
                Collections.sort(data, new NameComparator(val));
                leadsAdapter.notifyDataSetChanged();
            }
        });
        recyclerView.setAdapter(leadsAdapter);

        swipeRefreshLayout.setColorSchemeResources(R.color.app_color);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (Utilities.getInstance().isNetworkAvailable(getActivity()))
            getLeadDetails();
        else
            Utilities.getInstance().showSnackBarShort(getActivity(),getString(R.string.networkstatus),mCoordinatorLayout);
    }

    private void getLeadDetails(){
        loadingView =   Utilities.loadingView(getActivity(),"Loading");

        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        LeadRequestModel mLeadRequestModel = new LeadRequestModel();
        mLeadRequestModel.setMuid(CRMSharedPreference.getInstance(getActivity()).getString(CRMSessionConstants.UID));
        mLeadRequestModel.setStart(0);
        mLeadRequestModel.setLength(2);

        Call<LeadsModel> responseBodyCall = webServiceAPI.leads(mLeadRequestModel);
        responseBodyCall.enqueue(new Callback<LeadsModel>() {
            @Override
            public void onResponse(Call<LeadsModel> call, Response<LeadsModel> response) {
                loadingView.dismiss();
                Log.d(TAG ,"Result:::::::::: "+response.errorBody()+" "+ new Gson().toJson(response.code() )+ "     "+ new Gson().toJson(response.body()));

                if(response.body().getSuccess()){

                    data.clear();
                    data.addAll(response.body().getResult().getData());

                    leadsAdapter.notifyDataSetChanged();



                    if (data.size() == 0)
                        Utilities.getInstance().showSnackBarShort(getActivity(), "No items found",mCoordinatorLayout);                }
                else {
                    Utilities.getInstance().alertDialogSingleOptions(getActivity(), getString(R.string.invalid_user));

                }
            }
            @Override
            public void onFailure(Call<LeadsModel> call, Throwable t) {
                loadingView.dismiss();
                Utilities.getInstance().showSnackBarShort(getActivity(), t.getMessage(),mCoordinatorLayout);
            }
        });

        Utilities.getInstance().closeKeyboard(getActivity());
    }

    @Override
    public void onItemClick(View view, String val) {
Log.d(TAG,"Item Click Lead:::::::"+val );
    }

    @Override
    public void onResume() {
        super.onResume();
//        StopSer
    }
}
