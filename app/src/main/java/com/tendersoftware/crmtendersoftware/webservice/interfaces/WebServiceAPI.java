package com.tendersoftware.crmtendersoftware.webservice.interfaces;

import com.tendersoftware.crmtendersoftware.models.clients.ClientModel;
import com.tendersoftware.crmtendersoftware.models.clients.ClientRequestModel;
import com.tendersoftware.crmtendersoftware.models.comments.CommentGetResponseModel;
import com.tendersoftware.crmtendersoftware.models.comments.CommentRequestModel;
import com.tendersoftware.crmtendersoftware.models.comments.CommentResponseModel;
import com.tendersoftware.crmtendersoftware.models.comments.CommentsGetRequestModel;
import com.tendersoftware.crmtendersoftware.models.feeds.FeedsModel;
import com.tendersoftware.crmtendersoftware.models.feeds.FeedsRequestModel;
import com.tendersoftware.crmtendersoftware.models.internals.InternalModel;
import com.tendersoftware.crmtendersoftware.models.internals.InternalRequestModel;
import com.tendersoftware.crmtendersoftware.models.leads.LeadRequestModel;
import com.tendersoftware.crmtendersoftware.models.login.LoginModel;
import com.tendersoftware.crmtendersoftware.models.leads.LeadsModel;
import com.tendersoftware.crmtendersoftware.models.login.LoginRequestModel;
import com.tendersoftware.crmtendersoftware.models.login.LogoutModel;
import com.tendersoftware.crmtendersoftware.models.tasks.TaskModel;
import com.tendersoftware.crmtendersoftware.models.tasks.TaskRequestModel;
import com.tendersoftware.crmtendersoftware.models.tickets.TicketModel;
import com.tendersoftware.crmtendersoftware.models.tickets.TicketRequestModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by SYS on 9/14/2017.
 */

public interface WebServiceAPI {

    //Login Api
    @POST("login")
    Call<LoginModel> login(@Body LoginRequestModel params);

    //Logout Api
    @POST("logout")
    Call<LoginModel> logout(@Body LogoutModel params);

//    //Leads/Clients/Feeds/Internals Api
//    @POST("web/api/{path_value}")
//    Call<LeadsModel> getListDetails(@Body Map<String, Object> params, @Path(WebServiceConstants.PATH_VALUE));

    //Leads Api
    @POST("leads")
    Call<LeadsModel> leads(@Body LeadRequestModel params);
//    Call<LeadsModel> leads(@Body Map<String, Object> params);

    //Feeds Api
    @POST("feeds")
    Call<FeedsModel> feeds(@Body FeedsRequestModel params);

    //Clients Api
    @POST("clients")
    Call<ClientModel> clients(@Body ClientRequestModel params);

    //Internals Api
    @POST("internals")
    Call<InternalModel> internals(@Body InternalRequestModel params);
    //Ticket Api
    @POST("tickets")
    Call<TicketModel> tickets(@Body TicketRequestModel params);

    //Ticket Api
    @POST("tasks")
    Call<TaskModel> tasks(@Body TaskRequestModel params);

    //Forget Password
    @POST("forget_password")
    @FormUrlEncoded
    Call<LoginModel> forgetPassword(@Field("email") String email);

    //add comment for client
    @POST("add_comments")
    Call<CommentResponseModel> addComments(@Body CommentRequestModel email);

    //get comment for client
    @POST("get_comments")
    Call<CommentGetResponseModel> getComments(@Body CommentsGetRequestModel email);
}
