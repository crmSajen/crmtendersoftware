package com.tendersoftware.crmtendersoftware.database;

import android.util.Log;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;


public class DBMigration implements RealmMigration {

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();
        Log.e("oldVersion :: ",""+oldVersion);
        Log.e("newVersion :: ",""+newVersion);
        if (oldVersion == 1) {
            oldVersion++;
        }
    }
}

