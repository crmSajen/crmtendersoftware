package com.tendersoftware.crmtendersoftware.database.user;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by SYS on 9/28/2017.
 */

public class UserModelDatabase extends RealmObject{

    @PrimaryKey
    private int id;
    private String emailId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
