package com.tendersoftware.crmtendersoftware.database;

import android.content.Context;
import android.util.Log;

import com.tendersoftware.crmtendersoftware.database.user.UserModelDatabase;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmDBHelper {

    private static final RealmDBHelper ourInstance = new RealmDBHelper();

    public static RealmDBHelper getInstance() {
        return ourInstance;
    }

    private RealmDBHelper() {
    }

    public void deleteDatabase() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }

    public void deleteDatabase(Class c) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(c);
        realm.commitTransaction();
    }

    public void setDBConfiguration(Context context) {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("crmtendersoft.realm")
                .schemaVersion(1)
                .migration(new DBMigration())
                .build();
        Realm.setDefaultConfiguration(config);
    }


    public void insertEmailId(String emailId){
        if (getemailId(emailId)==null) {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            UserModelDatabase userModelDatabase = realm.createObject(UserModelDatabase.class,
                    getNextKey(realm, UserModelDatabase.class));
            userModelDatabase.setEmailId(emailId);
            realm.commitTransaction();
        }
    }

    public int getNextKey(Realm realm, Class c) {
        return realm.where(c).findAll().size() > 0 ? realm.where(c).max("id").intValue() +
                1 : 0;
    }

    public UserModelDatabase getemailId(String emailId){
        Realm realm = Realm.getDefaultInstance();
        UserModelDatabase  userModelDatabase =realm.where(UserModelDatabase.class).equalTo("emailId",emailId).findFirst();
        return userModelDatabase;
    }

    public List<String> getEmailL(){
        Realm realm = Realm.getDefaultInstance();
        List<UserModelDatabase> userModelDatabase =realm.where(UserModelDatabase.class).findAll();
        ArrayList<String> strings = new ArrayList<>();
        for(int i =0;i<userModelDatabase.size();i++){
            strings.add(i,userModelDatabase.get(i).getEmailId());
        }
        return strings;
    }

}