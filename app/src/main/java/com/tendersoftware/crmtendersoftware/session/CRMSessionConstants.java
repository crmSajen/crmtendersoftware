package com.tendersoftware.crmtendersoftware.session;


public class CRMSessionConstants {

    public static final String USER_ID = "user_id";
    public static final String UID = "u_id";
    public static final String IS_LOGIN = "is_login";
}
