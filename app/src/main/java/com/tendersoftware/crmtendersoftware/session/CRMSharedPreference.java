package com.tendersoftware.crmtendersoftware.session;

import android.content.Context;
import android.content.SharedPreferences;


public class CRMSharedPreference {

    private static CRMSharedPreference btwSession = null;
    private final String SHARED_PERFERENCE_NAME = "CRM_SESSION_VALUES";
    private final String DEFAULT_STRING_VALUE = "";
    private final int DEFAULT_INT_VALUE = -1;
    private final Boolean DEFAULT_BOOLEAN_VALUE = false;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    public static CRMSharedPreference getInstance(Context context) {

        if (btwSession == null) {
            btwSession = new CRMSharedPreference();
        }
        if (btwSession.getSharedPreferences() == null) {
            btwSession.setSharedPreferences(context);
        }

        return btwSession;
    }

    private SharedPreferences getSharedPreferences() {
        return sharedPref;
    }

    private void setSharedPreferences(Context context) {
        sharedPref = context.getSharedPreferences(SHARED_PERFERENCE_NAME,
                Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    public void putString(String TAG, String Value) {
        editor.putString(TAG, Value);
        editor.commit();
    }


    public String getString(String TAG) {
        return sharedPref.getString(TAG, DEFAULT_STRING_VALUE);
    }

    public void putInt(String TAG, int Value) {
        editor.putInt(TAG, Value);
        editor.commit();
    }

    public int getInt(String TAG) {
        return sharedPref.getInt(TAG, DEFAULT_INT_VALUE);
    }

    public void putBoolean(String TAG, Boolean Value) {
        editor.putBoolean(TAG, Value);
        editor.commit();
    }

    public Boolean getBoolean(String TAG) {
        return sharedPref.getBoolean(TAG, DEFAULT_BOOLEAN_VALUE);
    }
    public Boolean getBoolean(String TAG,boolean defualt_value) {
        return sharedPref.getBoolean(TAG, defualt_value);
    }


    public void clearAllSession() {
        editor.clear();
        editor.commit();
    }

    public void removeValues(String TAG) {
        editor.remove(TAG);
        editor.commit();
    }

}
