package com.tendersoftware.crmtendersoftware.interfaces;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by SYS on 10/4/2017.
 */

public interface AlertDialogInterface {

    void clickedOkButton(int code);
    void clickedCancelButton(int code);
}
