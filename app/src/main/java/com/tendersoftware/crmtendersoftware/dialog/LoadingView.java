package com.tendersoftware.crmtendersoftware.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.utils.Constants;


public class LoadingView extends DialogFragment {
  public LoadingView() {

  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
     text = getArguments().getString(Constants.DIALOG_MESSAGE);
  }

  Animation operatingAnim;

  Dialog mDialog;

  View mouse;

  TextView mGraduallyTextView;

  String text;

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    if (mDialog == null) {
      mDialog = new Dialog(getActivity(), R.style.loading_dialog);
      mDialog.setContentView(R.layout.catloading_main);
      mDialog.setCanceledOnTouchOutside(false);
      mDialog.setCancelable(false);
      mDialog.getWindow().setDimAmount(0.85f);
     // mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#45484d")));
      mDialog.getWindow().setGravity(Gravity.CENTER);

      operatingAnim = new RotateAnimation(360f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
      operatingAnim.setStartOffset(0);
     // operatingAnim.setRepeatMode(Animation.ABSOLUTE);
      operatingAnim.setRepeatCount(Animation.INFINITE);
      operatingAnim.setDuration(2000);

      LinearInterpolator lin = new LinearInterpolator();
      operatingAnim.setInterpolator(lin);


      View view = mDialog.getWindow().getDecorView();

      mouse = view.findViewById(R.id.mouse);


      mGraduallyTextView = (TextView) view.findViewById(R.id.textView);
      
      if (!TextUtils.isEmpty(text)) {
        mGraduallyTextView.setText(text);
      }

      operatingAnim.setAnimationListener(new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
      });
    }
    return mDialog;
  }

  @Override
  public void onResume() {
    super.onResume();
    mouse.setAnimation(operatingAnim);

//    mDialog.setOnKeyListener(new DialogInterface.OnKeyListener()
//    {
//      @Override
//      public boolean onKey(android.content.DialogInterface dialog,
//                           int keyCode,android.view.KeyEvent event)
//      {
//          return (keyCode == android.view.KeyEvent.KEYCODE_BACK);
//          //        // Otherwise, do nothing else
////        else return false;
//      }
//    });
  //  mGraduallyTextView.startLoading();
  }

  @Override
  public void onPause() {
    super.onPause();

    operatingAnim.reset();

    mouse.clearAnimation();

  //  mGraduallyTextView.stopLoading();
  }

  public void setText(String str) {
    text = str;
  }

  @Override
  public void onDismiss(DialogInterface dialog) {
    super.onDismiss(dialog);
    mDialog = null;
    System.gc();
  }
}
