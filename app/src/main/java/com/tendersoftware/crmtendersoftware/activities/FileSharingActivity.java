package com.tendersoftware.crmtendersoftware.activities;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.tendersoftware.crmtendersoftware.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FileSharingActivity extends AppCompatActivity {

    @BindView(R.id.url_text)
    TextView urlText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_sharing);
        ButterKnife.bind(this);

        Intent intent = getIntent();

        String receivedAction = intent.getAction();
        String receivedType = intent.getType();

        if (receivedAction.equals(Intent.ACTION_SEND)) {

            Uri audioUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);

            System.out.println("audioUri = " + audioUri);

            if (receivedType.startsWith("audio/")) {
                if (audioUri != null) {
                    urlText.setText(audioUri.toString());

                    audioPlayer(audioUri);
                }
            } else
                urlText.setText("NOT FOUND");
        }
    }

    public void audioPlayer(Uri path){
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(getApplicationContext(), path);
            mediaPlayer.prepare();
            mediaPlayer.start();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
