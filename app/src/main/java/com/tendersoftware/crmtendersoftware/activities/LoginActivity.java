package com.tendersoftware.crmtendersoftware.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.tendersoftware.crmtendersoftware.MainActivity;
import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.database.RealmDBHelper;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.fragments.ForgotPasswordDialog;
import com.tendersoftware.crmtendersoftware.models.login.LoginModel;
import com.tendersoftware.crmtendersoftware.models.login.LoginRequestModel;
import com.tendersoftware.crmtendersoftware.session.CRMSessionConstants;
import com.tendersoftware.crmtendersoftware.session.CRMSharedPreference;
import com.tendersoftware.crmtendersoftware.utils.DrawableClickListener;
import com.tendersoftware.crmtendersoftware.utils.Utilities;
import com.tendersoftware.crmtendersoftware.webservice.WebService;
import com.tendersoftware.crmtendersoftware.webservice.interfaces.WebServiceAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.login_edittext)
    AutoCompleteTextView loginEditText;
    @BindView(R.id.password_edittext)
    EditText passwordEditText;
    @BindView(R.id.layout)
    RelativeLayout viewLayout;
    @BindView(R.id.logo_img)
    ImageView imageView;

//    SystemProgressDialog systemProgressDialog;

    LoadingView loadingView;

    GoogleApiClient mCredentialsApiClient;
    CredentialRequest mCredentialRequest;
    private final String TAG= "SplashScreen";
    Response<LoginModel> responseModel;
    private String email = "",password ="";
    private List<String> userModelDatabases = new ArrayList<>();
    ArrayAdapter adapter;
    private Boolean VISIBLE_PASSWORD = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

//        if(getIntent().getExtras()!=null){
//            email = getIntent().getExtras().getString("EMAIL");
//            password = getIntent().getExtras().getString("PASSWORD");
//        }
//
//        loginEditText.setText(email);
//        passwordEditText.setText(password);

//        mCredentialsApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .enableAutoManage(this, this)
//                .addApi(Auth.CREDENTIALS_API)
//                .build();

        getDatabaseValues();

        adapter = new ArrayAdapter(this,R.layout.spinner_textview,userModelDatabases);
        loginEditText.setAdapter(adapter);
        loginEditText.setThreshold(1);
        passwordEditText.setOnTouchListener( new DrawableClickListener.RightDrawableClickListener(passwordEditText)
        {
            @Override
            public boolean onDrawableClick()
            {
                Log.d(TAG,"Show Password :::::");

                if (VISIBLE_PASSWORD) {
                    VISIBLE_PASSWORD = false;
                    passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordEditText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password, 0, R.drawable.ic_remove_red_eye_black_24dp, 0);
                } else {
                    VISIBLE_PASSWORD = true;
                    if(passwordEditText.getText().toString().length()> 0){
                        passwordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        passwordEditText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password, 0, R.drawable.ic_visibility_off_black_24dp, 0);
                    }
                }




                return true;
            }
        } );
    }

    private void getDatabaseValues(){
        userModelDatabases.addAll(RealmDBHelper.getInstance().getEmailL());
    }

    private void signinPassword(Credential credential){

        Auth.CredentialsApi.save(mCredentialsApiClient, credential).setResultCallback(
                new ResultCallback() {
                    @Override
                    public void onResult(Result result) {
                        Status status = result.getStatus();
                        if (status.isSuccess()) {
                            storePreferencesAndNavigate();
                        } else {
                            if (status.hasResolution()) {
                                // Try to resolve the save request. This will prompt the user if
                                // the credential is new.
                                try {
                                    status.startResolutionForResult(LoginActivity.this, 2);
                                } catch (IntentSender.SendIntentException e) {
                                    // Could not resolve the request
                                    Log.e(TAG, "STATUS: Failed to send resolution.", e);
                                    Utilities.getInstance().showSnackBarShort(LoginActivity.this,"Save failed",viewLayout);
                                }
                            } else {
                                // Request has no resolution
                                Utilities.getInstance().showSnackBarShort(LoginActivity.this,"Save failed",viewLayout);
                            }
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

         if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                storePreferencesAndNavigate();
            } else {
                Log.e(TAG, "Credential Read: NOT OK ");
                Utilities.getInstance().showSnackBarShort(LoginActivity.this, "Credential Read Failed",viewLayout);
            }
        }
    }

    private void storePreferencesAndNavigate(){
        Log.d(TAG, "SAVE: OK");
        Utilities.getInstance().showSnackBarShort(LoginActivity.this,"Credentials saved",viewLayout);
        CRMSharedPreference.getInstance(LoginActivity.this).putString(CRMSessionConstants.USER_ID,responseModel.body().getResult().getUserid());
        CRMSharedPreference.getInstance(LoginActivity.this).putString(CRMSessionConstants.UID,responseModel.body().getResult().getUid());
        CRMSharedPreference.getInstance(LoginActivity.this).putBoolean(CRMSessionConstants.IS_LOGIN,true);

        RealmDBHelper.getInstance().insertEmailId(loginEditText.getText().toString());

        System.out.println("RealmDBHelper.getInstance().getemailId() = " + RealmDBHelper.getInstance().getemailId(loginEditText.getText().toString()));

        View sharedView = imageView;
        String transitionName = "logo_animate";
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(this, sharedView, transitionName);
            // start the new activity
            startActivity(intent, options.toBundle());
        }else
            startActivity(intent);
//        finish();
    }



    @OnClick(R.id.login_button)
    public void login(){
        if(loginValidation())
            if(Utilities.getInstance().isNetworkAvailable(this))
                sendLoginDetails();
            else
                Utilities.getInstance().showSnackBarShort(LoginActivity.this,getString(R.string.networkstatus),viewLayout);
    }

    private void saveLogin(){
        Credential credential = new Credential.Builder(loginEditText.getText().toString())
                .setPassword(passwordEditText.getText().toString())
                .build();
        signinPassword(credential );
    }

    private boolean loginValidation(){
        if(!Utilities.getInstance().isValidEmail(loginEditText.getText().toString())) {
            Utilities.getInstance().showSnackBarShort(LoginActivity.this,"Please enter valid Email-Id",viewLayout);
            return false;
        }
        else if(passwordEditText.getText().toString().trim().length()==0) {
            Utilities.getInstance().showSnackBarShort(LoginActivity.this,"Please enter valid Password",viewLayout);
            return false;
        }
        else if(passwordEditText.getText().toString().trim().length()<5) {
            Utilities.getInstance().showSnackBarShort(LoginActivity.this,"Password should not less than 5 characters",viewLayout);
            return false;
        }
        return true;
    }

    private void sendLoginDetails(){
        loadingView =   Utilities.loadingView(this,"Loading");

        LoginRequestModel mLoginRequestModel = new LoginRequestModel();
        mLoginRequestModel.setUsername(loginEditText.getText().toString());
        mLoginRequestModel.setPassword(passwordEditText.getText().toString());


        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        Call<LoginModel> responseBodyCall = webServiceAPI.login(mLoginRequestModel);
        responseBodyCall.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                loadingView.dismiss();
                if(response.body().getSuccess()){
                    responseModel = response;
//                    saveLogin();
                    storePreferencesAndNavigate();
                }
                else {
                    Utilities.getInstance().showSnackBarShort(LoginActivity.this, response.body().getError().getMessage(),viewLayout);
                }
            }
            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                loadingView.dismiss();
                Log.d(TAG,"error Login "+t.toString());
                Utilities.getInstance().showSnackBarShort(LoginActivity.this, "Something went wrong",viewLayout);
            }
        });
    }

    @OnClick(R.id.forgot_password_text)
    public void cantAccessAccount(){
        ForgotPasswordDialog forgotPasswordDialog = new ForgotPasswordDialog();
        // Show DialogFragment
        forgotPasswordDialog.show(getSupportFragmentManager(), "Forgot Password");
//        Utilities.getInstance().showSnackBarShort(LoginActivity.this, "Work under progress!",viewLayout);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
