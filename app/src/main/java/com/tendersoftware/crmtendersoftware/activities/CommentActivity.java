package com.tendersoftware.crmtendersoftware.activities;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.tendersoftware.crmtendersoftware.R;
import com.tendersoftware.crmtendersoftware.adapter.CommentAdapter;
import com.tendersoftware.crmtendersoftware.dialog.LoadingView;
import com.tendersoftware.crmtendersoftware.models.comments.CommentDataModel;
import com.tendersoftware.crmtendersoftware.models.comments.CommentGetResponseModel;
import com.tendersoftware.crmtendersoftware.models.comments.CommentRequestModel;
import com.tendersoftware.crmtendersoftware.models.comments.CommentResponseModel;
import com.tendersoftware.crmtendersoftware.models.comments.CommentsGetRequestModel;
import com.tendersoftware.crmtendersoftware.models.feeds.DataModel;
import com.tendersoftware.crmtendersoftware.models.login.LoginModel;
import com.tendersoftware.crmtendersoftware.models.login.LoginRequestModel;
import com.tendersoftware.crmtendersoftware.session.CRMSessionConstants;
import com.tendersoftware.crmtendersoftware.session.CRMSharedPreference;
import com.tendersoftware.crmtendersoftware.utils.Utilities;
import com.tendersoftware.crmtendersoftware.webservice.WebService;
import com.tendersoftware.crmtendersoftware.webservice.interfaces.WebServiceAPI;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by SYS on 11/24/2017.
 */

public class CommentActivity  extends AppCompatActivity{

    private static final String TAG = "";
    LoadingView loadingView;
    private CommentAdapter mCommentAdapter;
    private List<DataModel> data = new ArrayList<>();
    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.camera_image)
    ImageView mCameraImage;
    @BindView(R.id.attachment_image)
    ImageView mAttachmentImage;
    @BindView(R.id.send_image)
    ImageView mSendImage;
    @BindView(R.id.comment_edit)
    EditText mCommentEdit;
    private List<CommentDataModel> mCommentDataModel = new ArrayList<CommentDataModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        if (Utilities.getInstance().isNetworkAvailable(getApplicationContext()))
            getcomments();
        else
            Utilities.getInstance().showSnackBarShort(getApplicationContext(), getString(R.string.networkstatus), mCoordinatorLayout);
        uiSetup();

        mSendImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCommentEdit.getText().toString().trim().length() > 0) {
                    if (Utilities.getInstance().isNetworkAvailable(getApplicationContext()))
                        sendClientComment();
                    else
                        Utilities.getInstance().showSnackBarShort(getApplicationContext(), getString(R.string.networkstatus), mCoordinatorLayout);
                } else {
                    Utilities.getInstance().showSnackBarShort(getApplicationContext(), "Please Enter Comments", mCoordinatorLayout);

                }
            }
        });
    }

    private void getcomments() {
        mCommentDataModel = new ArrayList<CommentDataModel>();
        loadingView =   Utilities.loadingView(this,"Loading");
        Intent in = getIntent();
        Bundle b = in.getExtras();

        String module = b.getString("module");
        int id = b.getInt("module_id");

        CommentsGetRequestModel mCommentGetRequestModel = new CommentsGetRequestModel();
        mCommentGetRequestModel.setMuid(CRMSharedPreference.getInstance(getApplicationContext()).getString(CRMSessionConstants.UID));
        mCommentGetRequestModel.setId(id);
        mCommentGetRequestModel.setModule(module);
        mCommentGetRequestModel.setLimit("2");


        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        Call<CommentGetResponseModel> responseBodyCall = webServiceAPI.getComments(mCommentGetRequestModel);
        responseBodyCall.enqueue(new Callback<CommentGetResponseModel>() {
            @Override
            public void onResponse(Call<CommentGetResponseModel> call, Response<CommentGetResponseModel> response) {
                loadingView.dismiss();

                if(response.body().getSuccess()){
                    mCommentDataModel.addAll(response.body().getResult().getData());
                    mCommentAdapter.notifyDataSetChanged();
                    Log.d(TAG,"Result of get Comment ::::::::"+new Gson().toJson(mCommentDataModel));

                }
                else {
                Utilities.getInstance().showSnackBarShort(CommentActivity.this, "Comment not Found!",mCoordinatorLayout);
                }
            }
            @Override
            public void onFailure(Call<CommentGetResponseModel> call, Throwable t) {
                loadingView.dismiss();
                Log.d(TAG,"error Login "+t.toString());
                Utilities.getInstance().showSnackBarShort(CommentActivity.this, "Something went wrong",mCoordinatorLayout);
            }
        });
    }


    private void sendClientComment() {
        loadingView =   Utilities.loadingView(this,"Loading");
        Intent in = getIntent();
        Bundle b = in.getExtras();

        String module = b.getString("module");
        int id = b.getInt("module_id");

        CommentRequestModel mCommentRequestModel = new CommentRequestModel();
        mCommentRequestModel.setMuid(CRMSharedPreference.getInstance(getApplicationContext()).getString(CRMSessionConstants.UID));
        mCommentRequestModel.setComment(mCommentEdit.getText().toString().trim());
        mCommentRequestModel.setId(id);
        mCommentRequestModel.setModule(module);


        WebServiceAPI webServiceAPI = WebService.createService(WebServiceAPI.class);
        Call<CommentResponseModel> responseBodyCall = webServiceAPI.addComments(mCommentRequestModel);
        responseBodyCall.enqueue(new Callback<CommentResponseModel>() {
            @Override
            public void onResponse(Call<CommentResponseModel> call, Response<CommentResponseModel> response) {
                loadingView.dismiss();
                if(response.body().getSuccess()){
                    Utilities.getInstance().showSnackBarShort(CommentActivity.this, response.body().getMessage(),mCoordinatorLayout);
                    mCommentEdit.getText().clear();
                    getcomments();

                }
                else {
                    Utilities.getInstance().showSnackBarShort(CommentActivity.this, response.body().getMessage(),mCoordinatorLayout);
                }
            }
            @Override
            public void onFailure(Call<CommentResponseModel> call, Throwable t) {
                loadingView.dismiss();
                Log.d(TAG,"error Login "+t.toString());
                Utilities.getInstance().showSnackBarShort(CommentActivity.this, "Something went wrong",mCoordinatorLayout);
            }
        });
    }

    private void uiSetup(){
        mCommentAdapter = new CommentAdapter(CommentActivity.this,mCommentDataModel);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mCommentAdapter);

        swipeRefreshLayout.setColorSchemeResources(R.color.app_color);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {


                if (Utilities.getInstance().isNetworkAvailable(getApplicationContext()))
                    getcomments();
                else
                    Utilities.getInstance().showSnackBarShort(getApplicationContext(), getString(R.string.networkstatus),mCoordinatorLayout);

                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


}
